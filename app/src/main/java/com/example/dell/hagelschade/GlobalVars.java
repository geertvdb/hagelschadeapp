package com.example.dell.hagelschade;

import android.app.Application;

/**
 * Created by Dell on 19/06/2017.
 */

public class GlobalVars extends Application {

    private String glbKlantNaam;
    private boolean glbCanUseCamera;
    private boolean blnSwitch;


    public String getGlbKlantNaam() {
        return glbKlantNaam;
    }

    public void setGlbKlantNaam(String glbKlantNaam) {
        this.glbKlantNaam = glbKlantNaam;
    }

    public boolean isGlbCanUseCamera() {
        return glbCanUseCamera;
    }

    public void setGlbCanUseCamera(boolean glbCanUseCamera) {
        this.glbCanUseCamera = glbCanUseCamera;
    }

    public boolean isBlnSwitch() {
        return blnSwitch;
    }

    public void setBlnSwitch(boolean blnSwitch) {
        this.blnSwitch = blnSwitch;
    }
}
