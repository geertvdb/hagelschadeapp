package com.example.dell.hagelschade;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ParametersActivity extends AppCompatActivity {

    private EditText edtEenmaligeRust, edtVoorbereiding, edtArbeidszone, edtHolleRuimten, edtUurloonSchade, edtUurloonDeMontage;
    private Button btnParameters;
    private  SharedPreferences.Editor editor;
    private int totalParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parameters);

        initialize();

        btnParameters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnParameters.startAnimation(animation);

                String eenmaligeRust = edtEenmaligeRust.getText().toString();
                String voorbereiding = edtVoorbereiding.getText().toString();
                String arbeidsZone = edtArbeidszone.getText().toString();
                String holleRuimten = edtHolleRuimten.getText().toString();
                String uurloonHagelschade = edtUurloonSchade.getText().toString();
                String uurloonDeMontage = edtUurloonDeMontage.getText().toString();


                if( !eenmaligeRust.equals("") && !voorbereiding.equals("") && !arbeidsZone.equals("") && !holleRuimten.equals("")){
                    int rust = Integer.valueOf(eenmaligeRust);
                    int voorb = Integer.valueOf(voorbereiding);
                    int zone = Integer.valueOf(arbeidsZone);
                    int holleR = Integer.valueOf(holleRuimten);
                    int uurloonSchade = Integer.valueOf(uurloonHagelschade);
                    totalParams = rust + voorb + zone + holleR;

                    editor.putInt("totalParams", totalParams);
                    editor.putInt("EenmaligeRust", rust);
                    editor.putInt("Voorbereiding", voorb);
                    editor.putInt("ArbeidsZone", zone);
                    editor.putInt("HolleRuimten", holleR);
                    editor.putInt("UurloonHagelSchade",uurloonSchade);
                    editor.putInt("UurloonDeMontage", Integer.valueOf(uurloonDeMontage));
                    editor.commit();

                    finish();
                }else{
                    Toast.makeText(ParametersActivity.this, "Gelieve alle velden in te geven of 0", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initialize() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("ParamsPrefFile", MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();

        edtEenmaligeRust = (EditText) findViewById(R.id.edtEenmaligeRustTijd);
        edtVoorbereiding = (EditText) findViewById(R.id.edtVoorbereiding);
        edtArbeidszone = (EditText) findViewById(R.id.edtWerkenBovenArbeidsZone);
        edtHolleRuimten = (EditText) findViewById(R.id.edtHolleRuimten);
        edtUurloonSchade = (EditText) findViewById(R.id.edtUurloonSchade);
        edtUurloonDeMontage = (EditText) findViewById(R.id.edtUurloonDeMontage);
        btnParameters = (Button) findViewById(R.id.btnParameters);

        edtEenmaligeRust.setText( String.valueOf(pref.getInt("EenmaligeRust", 0)));
        edtVoorbereiding.setText( String.valueOf(pref.getInt("Voorbereiding", 0)));
        edtArbeidszone.setText( String.valueOf(pref.getInt("ArbeidsZone", 0)));
        edtHolleRuimten.setText( String.valueOf(pref.getInt("HolleRuimten", 0)));
        edtUurloonSchade.setText( String.valueOf(pref.getInt("UurloonHagelSchade", 0)));
        edtUurloonDeMontage.setText( String.valueOf(pref.getInt("UurloonDeMontage", 0)));
    }
}
