package com.example.dell.hagelschade.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dell.hagelschade.R;
import com.example.dell.hagelschade.model.Bestek;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 17/06/2017.
 */

public class CustomListWithThumbsAdapter extends ArrayAdapter<Bestek> {

    List<Bestek> dataList = new ArrayList<Bestek>();
    ArrayList<ArrayList<File>> allBestekPics = new ArrayList<>();
    Context context;

    public CustomListWithThumbsAdapter(Context context, List<Bestek> dataList, ArrayList<ArrayList<File>> allBestekPics) {
        super(context, R.layout.list_item_with_thumbs, dataList);
        this.dataList = dataList;
        this.context=context;
        this.allBestekPics = allBestekPics;

    }

    static class LayoutHandler {
        TextView tvNaamKlant;
        TextView tvMerkVoertuig;
        TextView tvDateTime;
        ImageView ivThumb1;
        ImageView ivThumb2;
        ImageView ivThumb3;
        ImageView ivThumb4;
        ImageView ivThumb5;

    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        CustomListWithThumbsAdapter.LayoutHandler layoutHandler;
        row = convertView;

        if(row == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            row = layoutInflater.inflate(R.layout.list_item_with_thumbs, parent, false);
            layoutHandler = new CustomListWithThumbsAdapter.LayoutHandler();
            layoutHandler.tvNaamKlant = (TextView) row.findViewById(R.id.tvItemKlantNaam);
            layoutHandler.tvMerkVoertuig = (TextView) row.findViewById(R.id.tvItemMerkVoertuig);
            layoutHandler.tvDateTime = (TextView) row.findViewById(R.id.tvItemDatum);
            layoutHandler.ivThumb1 = (ImageView) row.findViewById(R.id.thumb1);
            layoutHandler.ivThumb2 = (ImageView) row.findViewById(R.id.thumb2);
            layoutHandler.ivThumb3 = (ImageView) row.findViewById(R.id.thumb3);
            layoutHandler.ivThumb4 = (ImageView) row.findViewById(R.id.thumb4);
            layoutHandler.ivThumb5 = (ImageView) row.findViewById(R.id.thumb5);
            row.setTag(layoutHandler);
        }else {
            layoutHandler = (CustomListWithThumbsAdapter.LayoutHandler) row.getTag();
        }

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        row.startAnimation(animation);
        lastPosition = position;

        Bestek dataProvider = (Bestek) this.getItem(position); // return elk OBJECT van de list
        layoutHandler.tvNaamKlant.setText(dataProvider.getNaamKlant());
        layoutHandler.tvMerkVoertuig.setText(dataProvider.getMerkVoertuig());
        layoutHandler.tvDateTime.setText(dataProvider.getCurrentDateTime());

        ArrayList<File> listWithImages = allBestekPics.get(position);
        int size = listWithImages.size();
        if(size == 0){
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb1);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb2);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb3);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb4);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb5);
        }
        if(size == 1){
            Picasso.with(context).load(listWithImages.get(0)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb1);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb2);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb3);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb4);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb5);
        }
        if(size == 2){
            Picasso.with(context).load(listWithImages.get(0)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb1);
            Picasso.with(context).load(listWithImages.get(1)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb2);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb3);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb4);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb5);
        }
        if(size == 3){
            Picasso.with(context).load(listWithImages.get(0)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb1);
            Picasso.with(context).load(listWithImages.get(1)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb2);
            Picasso.with(context).load(listWithImages.get(2)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb3);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb4);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb5);
        }
        if(size == 4){
            Picasso.with(context).load(listWithImages.get(0)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb1);
            Picasso.with(context).load(listWithImages.get(1)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb2);
            Picasso.with(context).load(listWithImages.get(2)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb3);
            Picasso.with(context).load(listWithImages.get(3)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb4);
            Picasso.with(context).load(R.drawable.auto).into(layoutHandler.ivThumb5);
        }
        if(size >= 5){
            Picasso.with(context).load(listWithImages.get(0)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb1);
            Picasso.with(context).load(listWithImages.get(1)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb2);
            Picasso.with(context).load(listWithImages.get(2)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb3);
            Picasso.with(context).load(listWithImages.get(3)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb4);
            Picasso.with(context).load(listWithImages.get(4)).placeholder(R.drawable.auto).into(layoutHandler.ivThumb5);
        }



        return row;
    }
}
