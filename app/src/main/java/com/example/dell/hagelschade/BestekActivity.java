package com.example.dell.hagelschade;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dell.hagelschade.database.DatabaseHandler;
import com.example.dell.hagelschade.model.Bestek;
import com.liuguangqiang.cookie.CookieBar;

import java.io.File;

public class BestekActivity extends AppCompatActivity {

    private Button btnUpdate, btnDelete, btnCancel;
    private EditText edtNaamHersteller, edtAdresHersteller, edtPostcodeHersteller, edtGemeenteHersteller,
        edtOndernemingsNrHersteller, edtNaamKlant, edtTelefoonKlant, edtEmailKlant, edtVerzekeraarKlant,
        edtMerkVoertuig, edtModelVoertuig, edtNummerPlVoertuig, edtChassisNrVoertuig, edtKleurVoertuig,
    edtTotalParams, edtUurloonHagelschade, edtUurloonDeMontage, edtTotalAE, edtTotalAEDeMontage, edtDateTime,
    edtTotaleKostprijs;
    private CheckBox cbVerzekerd;
    private int bestek_id;
    private DatabaseHandler dbHandler;
    private SQLiteDatabase sqLiteDatabase;
    private String naamKlant;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private GlobalVars mApp;
    private  String currentDateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bestek);

        mApp = (GlobalVars) getApplicationContext();
        Intent intent = getIntent();
        bestek_id = intent.getIntExtra("bestek_id",0);
        String naamHersteller = intent.getStringExtra("naamHersteller");
        String adresHersteller = intent.getStringExtra("adresHersteller");
        String postcodeHersteller = intent.getStringExtra("postcodeHersteller");
        String gemeenteHersteller = intent.getStringExtra("gemeenteHersteller");
        String ondernemingsNrHersteller = intent.getStringExtra("ondernemingsNrHersteller");
        naamKlant = intent.getStringExtra("naamKlant");
        String telefoonKlant = intent.getStringExtra("telefoonKlant");
        String emailKlant = intent.getStringExtra("emailKlant");
        final String verzekeraarKlant = intent.getStringExtra("verzekeraarKlant");
        int _verzekerdKlant = intent.getIntExtra("verzekerdKlant",0);
        String merkVoertuig = intent.getStringExtra("merkVoertuig");
        String modelVoertuig = intent.getStringExtra("modelVoertuig");
        String nummerPlVoertuig = intent.getStringExtra("nummerPlVoertuig");
        String chassisNrVoertuig = intent.getStringExtra("chassisNrVoertuig");
        String kleurVoertuig = intent.getStringExtra("kleurVoertuig");
        int totalParams = intent.getIntExtra("totalParams",0);
        int uurloonHagelschade = intent.getIntExtra("uurloonHagelschade",0);
        int uurloonDeMontage = intent.getIntExtra("uurloonDeMontage", 0);
        int totalAE = intent.getIntExtra("totalAE", 0);
        int totalAEDeMontage = intent.getIntExtra("totalAEDeMontage", 0);
        currentDateTime = intent.getStringExtra("currentDateTime");
        int totaleKostprijs = intent.getIntExtra("totaleKostprijs", 0);

        edtNaamHersteller = (EditText) findViewById(R.id.edtNaamHerstellerBestek);
        edtAdresHersteller = (EditText) findViewById(R.id.edtAdresHerstellerBestek);
        edtPostcodeHersteller = (EditText) findViewById(R.id.edtPostcodeHerstellerBestek);
        edtOndernemingsNrHersteller = (EditText) findViewById(R.id.edtOndernemersNrHerstellerBestek);
        edtGemeenteHersteller = (EditText) findViewById(R.id.edtGemeenteHerstellerBestek);
        edtNaamKlant = (EditText) findViewById(R.id.edtNaamKlantBestek);
        edtTelefoonKlant = (EditText) findViewById(R.id.edtTelefoonKlantBestek);
        edtEmailKlant = (EditText) findViewById(R.id.edtEmailKlantBestek);
        edtVerzekeraarKlant = (EditText) findViewById(R.id.edtVerzekeraarKlantBestek);
        edtMerkVoertuig = (EditText) findViewById(R.id.edtMerkVoertuigBestek);
        edtModelVoertuig = (EditText) findViewById(R.id.edtModelVoertuigBestek);
        edtNummerPlVoertuig = (EditText) findViewById(R.id.edtNrPlaatVoertuigBestek);
        edtChassisNrVoertuig = (EditText) findViewById(R.id.edtChassisNrVoertuigBestek);
        edtKleurVoertuig = (EditText) findViewById(R.id.edtKleurVoertuigBestek);
        edtTotalParams = (EditText) findViewById(R.id.edtTotalParamsBestek);
        edtUurloonHagelschade = (EditText) findViewById(R.id.edtUurloonHagelschadeBestek);
        edtUurloonDeMontage = (EditText) findViewById(R.id.edtDeMontageBestek);
        edtTotalAE = (EditText) findViewById(R.id.edtTotalAEBestek);
        edtTotalAEDeMontage = (EditText) findViewById(R.id.edtTotalAEDemontageBestek);
        edtDateTime = (EditText) findViewById(R.id.edtDateTimeBestek);
        //edtDateTime.setEnabled(false);
        edtTotaleKostprijs = (EditText) findViewById(R.id.edtTotaleKostprijsBestek);
        cbVerzekerd = (CheckBox) findViewById(R.id.cbVerzekerdKlantBestek);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnCancel = (Button) findViewById(R.id.btnBack);

        edtNaamHersteller.setText(naamHersteller);
        edtAdresHersteller.setText(adresHersteller);
        edtPostcodeHersteller.setText(postcodeHersteller);
        edtGemeenteHersteller.setText(gemeenteHersteller);
        edtOndernemingsNrHersteller.setText(ondernemingsNrHersteller);
        edtNaamKlant.setText(naamKlant);
        edtTelefoonKlant.setText(telefoonKlant);
        edtEmailKlant.setText(emailKlant);
        edtVerzekeraarKlant.setText(verzekeraarKlant);
        edtMerkVoertuig.setText(merkVoertuig);
        edtModelVoertuig.setText(modelVoertuig);
        edtNummerPlVoertuig.setText(nummerPlVoertuig);
        edtChassisNrVoertuig.setText(chassisNrVoertuig);
        edtKleurVoertuig.setText(kleurVoertuig);
        edtTotalParams.setText(String.valueOf(totalParams));
        edtUurloonHagelschade.setText(uurloonHagelschade + "");
        edtUurloonDeMontage.setText(uurloonDeMontage + "");
        edtTotalAE.setText(totalAE + "");
        edtTotalAEDeMontage.setText(totalAEDeMontage + "");
        edtDateTime.setText(currentDateTime);
        edtTotaleKostprijs.setText(totaleKostprijs + "");
        if(_verzekerdKlant == 0){
            cbVerzekerd.setChecked(false);
        }else{
            cbVerzekerd.setChecked(true);
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnUpdate.startAnimation(animation);

                AlertDialog dialog = new AlertDialog.Builder(BestekActivity.this)
                        .setTitle("Update")
                        .setMessage("WIlt u het bestek updaten???")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Bestek bestek = new Bestek();
                                bestek.setNaamHersteller(edtNaamHersteller.getText().toString());
                                bestek.setAdresHersteller(edtAdresHersteller.getText().toString());
                                bestek.setPostcodeHersteller(edtPostcodeHersteller.getText().toString());
                                bestek.setGemeenteHersteller(edtGemeenteHersteller.getText().toString());
                                bestek.setOndernemingsNrHersteller(edtOndernemingsNrHersteller.getText().toString());
                                bestek.setNaamKlant(edtNaamKlant.getText().toString());
                                bestek.setTelefoonKlant(edtTelefoonKlant.getText().toString());
                                bestek.setEmailKlant(edtEmailKlant.getText().toString());
                                bestek.setVerzekeraarKlant(edtVerzekeraarKlant.getText().toString());
                                int verzekerdKlant;
                                if(cbVerzekerd.isChecked()){
                                    verzekerdKlant = 1;
                                }else{
                                    verzekerdKlant = 0;
                                }
                                bestek.setVerzekerdKlant(verzekerdKlant);
                                bestek.setMerkVoertuig(edtMerkVoertuig.getText().toString());
                                bestek.setModelVoertuig(edtModelVoertuig.getText().toString());
                                bestek.setNummerPlVoertuig(edtNummerPlVoertuig.getText().toString());
                                bestek.setChassisNrVoertuig(edtChassisNrVoertuig.getText().toString());
                                bestek.setKleurVoertuig(edtKleurVoertuig.getText().toString());
                                bestek.setTotalParams(Integer.valueOf(edtTotalParams.getText().toString()));
                                bestek.setUurloonHagelSchade(Integer.valueOf(edtUurloonHagelschade.getText().toString()));
                                bestek.setUurloonDemontage(Integer.valueOf(edtUurloonDeMontage.getText().toString()));
                                bestek.setTotalAE(Integer.valueOf(edtTotalAE.getText().toString()));
                                bestek.setTotalAEDeMontage(Integer.valueOf(edtTotalAEDeMontage.getText().toString()));
                                bestek.setTotaleKostprijs(Integer.valueOf(edtTotaleKostprijs.getText().toString()));
                                bestek.setCurrentDateTime(edtDateTime.getText().toString());

                                dbHandler = new DatabaseHandler(BestekActivity.this);
                                sqLiteDatabase = dbHandler.getWritableDatabase();
                                dbHandler.updateBestek(String.valueOf(bestek_id), bestek, sqLiteDatabase);

                                // rename photo map naar nieuwe naam klant om foto's van bestek te kunnen ophalen op naamklant
                                String newNameKlant = bestek.getNaamKlant();
                                mApp.setGlbKlantNaam(newNameKlant);
                                File sdcard = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "hagelschade");
                                Uri uri = Uri.fromFile(sdcard);
                                String filePath = FilePath.getPath(BestekActivity.this, uri);
                                File from = new File(sdcard,naamKlant);
                                File to = new File(sdcard,newNameKlant);
                                from.renameTo(to);

                                Intent intent = new Intent(BestekActivity.this, ListActivity.class);
                                intent.putExtra("NaamKlantBestek", bestek.getNaamKlant());
                                intent.putExtra("updateOrDelete","update");
                                setResult(Activity.RESULT_OK,intent);

                                finish();

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnDelete.startAnimation(animation);

                AlertDialog dialog = new AlertDialog.Builder(BestekActivity.this)
                        .setTitle("Delete")
                        .setMessage("Bestek verwijderen???")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dbHandler = new DatabaseHandler(BestekActivity.this);
                                sqLiteDatabase = dbHandler.getWritableDatabase();
                                dbHandler.deleteBestek(String.valueOf(bestek_id), sqLiteDatabase);
                                File imgPath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "hagelschade");
                                //Uri uri = Uri.fromFile(sdcard);
                                //String filePath = FilePath.getPath(BestekActivity.this, uri);
                                File dirToDelete = new File(imgPath,naamKlant);
                                boolean isDeleted = deleteDir(dirToDelete);
                                //path to pdf
                          /*      File pdfPath = new File(Environment.getExternalStorageDirectory(), "hagelschadePDF");
                                File pdfFileToDelete = new File(pdfPath, naamKlant + "_" +  currentDateTime + ".pdf");
                                if(pdfFileToDelete.exists()){
                                    pdfFileToDelete.delete();
                                }

                                //path to xls
                                File xlsPath = new File(Environment.getExternalStorageDirectory(), "hagelschadeXLS");
                                File xlsFileToDelete = new File(xlsPath, naamKlant + "_" + currentDateTime + ".xls");
                                if(xlsFileToDelete.exists()){
                                    xlsFileToDelete.delete();
                                } */


                                Intent intent = new Intent(BestekActivity.this, ListActivity.class);
                                intent.putExtra("NaamKlantBestek", naamKlant);
                                intent.putExtra("updateOrDelete","delete");
                                setResult(Activity.RESULT_OK,intent);

                                finish();

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnCancel.startAnimation(animation);

                Intent intent = new Intent(BestekActivity.this, ListActivity.class);
                intent.putExtra("NaamKlantBestek", "");
                intent.putExtra("updateOrDelete","");
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });
    }

    public boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
}
