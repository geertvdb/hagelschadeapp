package com.example.dell.hagelschade;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

public class ZoneActivity extends AppCompatActivity {

    // Zone Dak
    private EditText edt10Dak;
    private EditText edt20Dak;
    private EditText edt30Dak;
    private EditText edt40Dak;
    private EditText edtDeMontageDak;

    private CheckBox cbAluDak;
    private CheckBox cbKleefDak;

    // Zone Motorkap
    private EditText edt10Mo;
    private EditText edt20Mo;
    private EditText edt30Mo;
    private EditText edt40Mo;
    private EditText edtDeMontageMo;

    private CheckBox cbAluMo;
    private CheckBox cbKleefMo;

    // Zone Koffer/Achterklep
    private EditText edt10Koffer;
    private EditText edt20Koffer;
    private EditText edt30Koffer;
    private EditText edt40Koffer;
    private EditText edtDeMontageKoffer;

    private CheckBox cbAluKoffer;
    private CheckBox cbKleefKoffer;

    // Zone Achterpaneel
    private EditText edt10APaneel;
    private EditText edt20APaneel;
    private EditText edt30APaneel;
    private EditText edt40APaneel;
    private EditText edtDeMontageAPaneel;

    private CheckBox cbAluAPaneel;
    private CheckBox cbKleefAPaneel;

    // Zone Bumper Achter
    private EditText edt10BumperA;
    private EditText edt20BumperA;
    private EditText edt30BumperA;
    private EditText edt40BumperA;
    private EditText edtDeMontageBumperA;

    private CheckBox cbAluBumperA;
    private CheckBox cbKleefBumperA;

    // Zone Bumper Voor
    private EditText edt10BumperV;
    private EditText edt20BumperV;
    private EditText edt30BumperV;
    private EditText edt40BumperV;
    private EditText edtDeMontageBumperV;

    private CheckBox cbAluBumperV;
    private CheckBox cbKleefBumperV;

    // Zone Grille
    private EditText edt10Grille;
    private EditText edt20Grille;
    private EditText edt30Grille;
    private EditText edt40Grille;
    private EditText edtDeMontageGrille;

    private CheckBox cbAluGrille;
    private CheckBox cbKleefGrille;

    // Zone vleugel LV
    private EditText edt10VleugelLV;
    private EditText edt20VleugelLV;
    private EditText edt30VleugelLV;
    private EditText edt40VleugelLV;
    private EditText edtDeMontageVleugelLV;

    private CheckBox cbAluVleugelLV;
    private CheckBox cbKleefVleugelLV;

    // Zone DEUR LV
    private EditText edt10DeurLV;
    private EditText edt20DeurLV;
    private EditText edt30DeurLV;
    private EditText edt40DeurLV;
    private EditText edtDeMontageDeurLV;

    private CheckBox cbAluDeurLV;
    private CheckBox cbKleefDeurLV;

    // Zone DEUR LA
    private EditText edt10DeurLA;
    private EditText edt20DeurLA;
    private EditText edt30DeurLA;
    private EditText edt40DeurLA;
    private EditText edtDeMontageDeurLA;

    private CheckBox cbAluDeurLA;
    private CheckBox cbKleefDeurLA;

    // Zone vleugel LA
    private EditText edt10VleugelLA;
    private EditText edt20VleugelLA;
    private EditText edt30VleugelLA;
    private EditText edt40VleugelLA;
    private EditText edtDeMontageVleugelLA;

    private CheckBox cbAluVleugelLA;
    private CheckBox cbKleefVleugelLA;

    // Zone A/B/C Stijl L
    private EditText edt10StijlL;
    private EditText edt20StijlL;
    private EditText edt30StijlL;
    private EditText edt40StijlL;
    private EditText edtDeMontageStijlL;

    private CheckBox cbAluStijlL;
    private CheckBox cbKleefStijlL;

    // Zone vleugel RV
    private EditText edt10VleugelRV;
    private EditText edt20VleugelRV;
    private EditText edt30VleugelRV;
    private EditText edt40VleugelRV;
    private EditText edtDeMontageVleugelRV;

    private CheckBox cbAluVleugelRV;
    private CheckBox cbKleefVleugelRV;

    // Zone Deur RV
    private EditText edt10DeurRV;
    private EditText edt20DeurRV;
    private EditText edt30DeurRV;
    private EditText edt40DeurRV;
    private EditText edtDeMontageDeurRV;

    private CheckBox cbAluDeurRV;
    private CheckBox cbKleefDeurRV;

    // Zone Deur RA
    private EditText edt10DeurRA;
    private EditText edt20DeurRA;
    private EditText edt30DeurRA;
    private EditText edt40DeurRA;
    private EditText edtDeMontageDeurRA;

    private CheckBox cbAluDeurRA;
    private CheckBox cbKleefDeurRA;

    // Zone vleugel RA
    private EditText edt10VleugelRA;
    private EditText edt20VleugelRA;
    private EditText edt30VleugelRA;
    private EditText edt40VleugelRA;
    private EditText edtDeMontageVleugelRA;

    private CheckBox cbAluVleugelRA;
    private CheckBox cbKleefVleugelRA;

    // Zone A/B/C Stijl R
    private EditText edt10StijlR;
    private EditText edt20StijlR;
    private EditText edt30StijlR;
    private EditText edt40StijlR;
    private EditText edtDeMontageStijlR;

    private CheckBox cbAluStijlR;
    private CheckBox cbKleefStijlR;




    private int finalAEResult = 0;
    private int totalDakAE = 0;
    private int totalMoAE = 0;
    private int totalKofferAE = 0;
    private int totalAPaneelAE = 0;
    private int totalBumperAchterAE = 0;
    private int totalBumperVoorAE = 0;
    private int totalGrilleAE = 0;
    private int totalVleugelLVAE = 0;
    private int totalDeurLVAE = 0;
    private int totalDeurLAAE = 0;
    private int totalVleugelLAAE = 0;
    private int totalStijlLAE = 0;
    private int totalDeMontage = 0;
    private int totalVleugelRVAE = 0;
    private int totalDeurRVAE = 0;
    private int totalDeurRAAE = 0;
    private int totalVleugelRAAE = 0;
    private int totalStijlRAE = 0;

    private DecimalFormat oneDecimal = new DecimalFormat("#");
    private  SharedPreferences.Editor editor;


    private Button btnBerekenZonesAE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone);

        initialise();

        btnBerekenZonesAE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnBerekenZonesAE.startAnimation(animation);

                berekenResult();

                editor.putInt("TotaalHagelSchade", finalAEResult);
                editor.putInt("TotaalDeMontage", totalDeMontage);
                editor.commit();
            }
        });
    }

    private void initialise() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("ZonePrefFile", MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();

        // Zone Dak
        edt10Dak = (EditText) findViewById(R.id.et10);
        edt20Dak = (EditText) findViewById(R.id.et20);
        edt30Dak = (EditText) findViewById(R.id.et30);
        edt40Dak = (EditText) findViewById(R.id.et40);
        edtDeMontageDak = (EditText) findViewById(R.id.etDeMontage);

        // Zone Motorkap
        edt10Mo = (EditText) findViewById(R.id.et10M);
        edt20Mo = (EditText) findViewById(R.id.et20M);
        edt30Mo = (EditText) findViewById(R.id.et30M);
        edt40Mo = (EditText) findViewById(R.id.et40M);
        edtDeMontageMo = (EditText) findViewById(R.id.etDeMontageM);

        // Zone Koffer/Achterklep
        edt10Koffer = (EditText) findViewById(R.id.et10Koffer);
        edt20Koffer = (EditText) findViewById(R.id.et20Koffer);
        edt30Koffer = (EditText) findViewById(R.id.et30Koffer);
        edt40Koffer = (EditText) findViewById(R.id.et40Koffer);
        edtDeMontageKoffer = (EditText) findViewById(R.id.etDeMontageKoffer);

        // Zone Achterpaneel
        edt10APaneel = (EditText) findViewById(R.id.et10APaneel);
        edt20APaneel = (EditText) findViewById(R.id.et20APaneel);
        edt30APaneel= (EditText) findViewById(R.id.et30APaneel);
        edt40APaneel = (EditText) findViewById(R.id.et40APaneel);
        edtDeMontageAPaneel = (EditText) findViewById(R.id.etDeMontageAPaneel);

        // Zone Bumper Achter
        edt10BumperA = (EditText) findViewById(R.id.et10BumperA);
        edt20BumperA = (EditText) findViewById(R.id.et20BumperA);
        edt30BumperA= (EditText) findViewById(R.id.et30BumperA);
        edt40BumperA = (EditText) findViewById(R.id.et40BumperA);
        edtDeMontageBumperA = (EditText) findViewById(R.id.etDeMontageBumperA);

        // Zone Bumper Voor
        edt10BumperV = (EditText) findViewById(R.id.et10BumperV);
        edt20BumperV = (EditText) findViewById(R.id.et20BumperV);
        edt30BumperV = (EditText) findViewById(R.id.et30BumperV);
        edt40BumperV = (EditText) findViewById(R.id.et40BumperV);
        edtDeMontageBumperV = (EditText) findViewById(R.id.etDeMontageBumperV);

        // Zone Grille
        edt10Grille = (EditText) findViewById(R.id.et10Grille);
        edt20Grille = (EditText) findViewById(R.id.et20Grille);
        edt30Grille = (EditText) findViewById(R.id.et30Grille);
        edt40Grille = (EditText) findViewById(R.id.et40Grille);
        edtDeMontageGrille = (EditText) findViewById(R.id.etDeMontageGrille);

        // Zone vleugel LV
        edt10VleugelLV = (EditText) findViewById(R.id.et10VleugelLV);
        edt20VleugelLV = (EditText) findViewById(R.id.et20VleugelLV);
        edt30VleugelLV = (EditText) findViewById(R.id.et30VleugelLV);
        edt40VleugelLV = (EditText) findViewById(R.id.et40VleugelLV);
        edtDeMontageVleugelLV = (EditText) findViewById(R.id.etDeMontageVleugelLV);

        // Zone deur LV
        edt10DeurLV = (EditText) findViewById(R.id.et10DeurLV);
        edt20DeurLV = (EditText) findViewById(R.id.et20DeurLV);
        edt30DeurLV = (EditText) findViewById(R.id.et30DeurLV);
        edt40DeurLV = (EditText) findViewById(R.id.et40DeurLV);
        edtDeMontageDeurLV = (EditText) findViewById(R.id.etDeMontageDeurLV);

        // Zone deur LA
        edt10DeurLA = (EditText) findViewById(R.id.et10DeurLA);
        edt20DeurLA = (EditText) findViewById(R.id.et20DeurLA);
        edt30DeurLA = (EditText) findViewById(R.id.et30DeurLA);
        edt40DeurLA = (EditText) findViewById(R.id.et40DeurLA);
        edtDeMontageDeurLA = (EditText) findViewById(R.id.etDeMontageDeurLA);

        // Zone vleugel LA
        edt10VleugelLA = (EditText) findViewById(R.id.et10VleugelLA);
        edt20VleugelLA = (EditText) findViewById(R.id.et20VleugelLA);
        edt30VleugelLA = (EditText) findViewById(R.id.et30VleugelLA);
        edt40VleugelLA = (EditText) findViewById(R.id.et40VleugelLA);
        edtDeMontageVleugelLA = (EditText) findViewById(R.id.etDeMontageVleugelLA);

        // Zone A/B/C Stijl L
        edt10StijlL = (EditText) findViewById(R.id.et10StijlL);
        edt20StijlL = (EditText) findViewById(R.id.et20StijlL);
        edt30StijlL = (EditText) findViewById(R.id.et30StijlL);
        edt40StijlL = (EditText) findViewById(R.id.et40StijlL);
        edtDeMontageStijlL = (EditText) findViewById(R.id.etDeMontageStijlL);

        // Zone vleugel RV
        edt10VleugelRV = (EditText) findViewById(R.id.et10VleugelRV);
        edt20VleugelRV = (EditText) findViewById(R.id.et20VleugelRV);
        edt30VleugelRV = (EditText) findViewById(R.id.et30VleugelRV);
        edt40VleugelRV = (EditText) findViewById(R.id.et40VleugelRV);
        edtDeMontageVleugelRV= (EditText) findViewById(R.id.etDeMontageVleugelRV);

        // Zone deur RV
        edt10DeurRV = (EditText) findViewById(R.id.et10DeurRV);
        edt20DeurRV = (EditText) findViewById(R.id.et20DeurRV);
        edt30DeurRV = (EditText) findViewById(R.id.et30DeurRV);
        edt40DeurRV = (EditText) findViewById(R.id.et40DeurRV);
        edtDeMontageDeurRV = (EditText) findViewById(R.id.etDeMontageDeurRV);

        // Zone deur RA
        edt10DeurRA = (EditText) findViewById(R.id.et10DeurRA);
        edt20DeurRA = (EditText) findViewById(R.id.et20DeurRA);
        edt30DeurRA = (EditText) findViewById(R.id.et30DeurRA);
        edt40DeurRA = (EditText) findViewById(R.id.et40DeurRA);
        edtDeMontageDeurRA = (EditText) findViewById(R.id.etDeMontageDeurRA);

        // Zone vleugel RA
        edt10VleugelRA = (EditText) findViewById(R.id.et10VleugelRA);
        edt20VleugelRA = (EditText) findViewById(R.id.et20VleugelRA);
        edt30VleugelRA = (EditText) findViewById(R.id.et30VleugelRA);
        edt40VleugelRA = (EditText) findViewById(R.id.et40VleugelRA);
        edtDeMontageVleugelRA= (EditText) findViewById(R.id.etDeMontageVleugelRA);

        // Zone A/B/C Stijl R
        edt10StijlR = (EditText) findViewById(R.id.et10StijlR);
        edt20StijlR = (EditText) findViewById(R.id.et20StijlR);
        edt30StijlR = (EditText) findViewById(R.id.et30StijlR);
        edt40StijlR = (EditText) findViewById(R.id.et40StijlR);
        edtDeMontageStijlR = (EditText) findViewById(R.id.etDeMontageStijlR);

        btnBerekenZonesAE = (Button) findViewById(R.id.btnBerekenZonesAE);

        //dak
        cbAluDak = (CheckBox) findViewById(R.id.cbAlu);
        cbKleefDak = (CheckBox) findViewById(R.id.cbKleef);

        //motorkap
        cbAluMo = (CheckBox) findViewById(R.id.cbAluM);
        cbKleefMo = (CheckBox) findViewById(R.id.cbKleefM);

        //koffer
        cbAluKoffer = (CheckBox) findViewById(R.id.cbAluKoffer);
        cbKleefKoffer = (CheckBox) findViewById(R.id.cbKleefKoffer);

        //achterpaneel
        cbAluAPaneel = (CheckBox) findViewById(R.id.cbAluAPaneel);
        cbKleefAPaneel = (CheckBox) findViewById(R.id.cbKleefAPaneel);

        //bumper achter
        cbAluBumperA = (CheckBox) findViewById(R.id.cbAluBumperA);
        cbKleefBumperA = (CheckBox) findViewById(R.id.cbKleefBumperA);

        //zone bumper voor
        cbAluBumperV = (CheckBox) findViewById(R.id.cbAluBumperV);
        cbKleefBumperV = (CheckBox) findViewById(R.id.cbKleefBumperV);

        // Zone grille
        cbAluGrille = (CheckBox) findViewById(R.id.cbAluGrille);
        cbKleefGrille = (CheckBox) findViewById(R.id.cbKleefGrille);

        // zone Vleugel LV
        cbAluVleugelLV = (CheckBox) findViewById(R.id.cbAluVleugelLV);
        cbKleefVleugelLV = (CheckBox) findViewById(R.id.cbKleefVleugelLV);

        // zone DEUR LV
        cbAluDeurLV = (CheckBox) findViewById(R.id.cbAluDeurLV);
        cbKleefDeurLV = (CheckBox) findViewById(R.id.cbKleefDeurLV);

        // zone DEUR LA
        cbAluDeurLA = (CheckBox) findViewById(R.id.cbAluDeurLA);
        cbKleefDeurLA = (CheckBox) findViewById(R.id.cbKleefDeurLA);

        // zone Vleugel LA
        cbAluVleugelLA = (CheckBox) findViewById(R.id.cbAluVleugelLA);
        cbKleefVleugelLA = (CheckBox) findViewById(R.id.cbKleefVleugelLA);

        // zone A/B/C Stijl L
        cbAluStijlL = (CheckBox) findViewById(R.id.cbAluStijlL);
        cbKleefStijlL = (CheckBox) findViewById(R.id.cbKleefStijlL);

        // zone Vleugel RV
        cbAluVleugelRV = (CheckBox) findViewById(R.id.cbAluVleugelRV);
        cbKleefVleugelRV = (CheckBox) findViewById(R.id.cbKleefVleugelRV);

        // zone DEUR RV
        cbAluDeurRV = (CheckBox) findViewById(R.id.cbAluDeurRV);
        cbKleefDeurRV = (CheckBox) findViewById(R.id.cbKleefDeurRV);

        // zone DEUR RA
        cbAluDeurRA = (CheckBox) findViewById(R.id.cbAluDeurRA);
        cbKleefDeurRA = (CheckBox) findViewById(R.id.cbKleefDeurRA);

        // zone Vleugel RA
        cbAluVleugelRA = (CheckBox) findViewById(R.id.cbAluVleugelRA);
        cbKleefVleugelRA = (CheckBox) findViewById(R.id.cbKleefVleugelRA);

        // zone A/B/C Stijl R
        cbAluStijlR = (CheckBox) findViewById(R.id.cbAluStijlR);
        cbKleefStijlR = (CheckBox) findViewById(R.id.cbKleefStijlR);
    }

    //format double value
    public String formatValue(float value) {
        return oneDecimal.format(value);
    }

    // Test methode format
    public int format(float f) {
        int c = (int) ((f) + 0.5f);
        float n = f + 0.5f;
        return (n - c) % 2 == 0 ? (int) f : c;
    }


    private void berekenResult() {

        //ZONE DAK
        String strDak10 = edt10Dak.getText().toString();
        String strDak20 = edt20Dak.getText().toString();
        String strDak30 = edt30Dak.getText().toString();
        String strDak40 = edt40Dak.getText().toString();
        String strDakDeMontage = edtDeMontageDak.getText().toString();

        //ZONE MOTORKAP
        String strMo10 = edt10Mo.getText().toString();
        String strMo20 = edt20Mo.getText().toString();
        String strMo30 = edt30Mo.getText().toString();
        String strMo40 = edt40Mo.getText().toString();
        String strMoDeMontage = edtDeMontageMo.getText().toString();

        // Zone koffer/achterklep
        String strKof10 = edt10Koffer.getText().toString();
        String strKof20 = edt20Koffer.getText().toString();
        String strKof30 = edt30Koffer.getText().toString();
        String strKof40 = edt40Koffer.getText().toString();
        String strKofferDeMontage = edtDeMontageKoffer.getText().toString();

        //ZONE ACHTERPANEEL
        String strAPaneel10 = edt10APaneel.getText().toString();
        String strAPaneel20 = edt20APaneel.getText().toString();
        String strAPaneel30 = edt30APaneel.getText().toString();
        String strAPaneel40 = edt40APaneel.getText().toString();
        String strAPaneelDeMontage = edtDeMontageAPaneel.getText().toString();

        // ZONE BUMPER ACHTER
        String strBumperA10 = edt10BumperA.getText().toString();
        String strBumperA20 = edt20BumperA.getText().toString();
        String strBumperA30 = edt30BumperA.getText().toString();
        String strBumperA40 = edt40BumperA.getText().toString();
        String strBumperADeMontage = edtDeMontageBumperA.getText().toString();

        // ZONE BUMPER VOOR
        String strBumperV10 = edt10BumperV.getText().toString();
        String strBumperV20 = edt20BumperV.getText().toString();
        String strBumperV30 = edt30BumperV.getText().toString();
        String strBumperV40 = edt40BumperV.getText().toString();
        String strBumperVDeMontage = edtDeMontageBumperV.getText().toString();

        // ZONE GRILLE
        String strGrille10 = edt10Grille.getText().toString();
        String strGrille20 = edt20Grille.getText().toString();
        String strGrille30 = edt30Grille.getText().toString();
        String strGrille40 = edt40Grille.getText().toString();
        String strGrilleDeMontage = edtDeMontageGrille.getText().toString();

        // ZONE VLEUGEL LV
        String strVleugelLV10 = edt10VleugelLV.getText().toString();
        String strVleugelLV20 = edt20VleugelLV.getText().toString();
        String strVleugelLV30 = edt30VleugelLV.getText().toString();
        String strVleugelLV40 = edt40VleugelLV.getText().toString();
        String strVleugelLVDeMontage = edtDeMontageVleugelLV.getText().toString();

        // ZONE DEUR LV
        String strDeurLV10 = edt10DeurLV.getText().toString();
        String strDeurLV20 = edt20DeurLV.getText().toString();
        String strDeurLV30 = edt30DeurLV.getText().toString();
        String strDeurLV40 = edt40DeurLV.getText().toString();
        String strDeurLVDeMontage = edtDeMontageDeurLV.getText().toString();

        // ZONE DEUR LA
        String strDeurLA10 = edt10DeurLA.getText().toString();
        String strDeurLA20 = edt20DeurLA.getText().toString();
        String strDeurLA30 = edt30DeurLA.getText().toString();
        String strDeurLA40 = edt40DeurLA.getText().toString();
        String strDeurLADeMontage = edtDeMontageDeurLA.getText().toString();

        // ZONE VLEUGEL LA
        String strVleugelLA10 = edt10VleugelLA.getText().toString();
        String strVleugelLA20 = edt20VleugelLA.getText().toString();
        String strVleugelLA30 = edt30VleugelLA.getText().toString();
        String strVleugelLA40 = edt40VleugelLA.getText().toString();
        String strVleugelLADeMontage = edtDeMontageVleugelLA.getText().toString();

        // ZONE A/B/C Stijl L
        String strStijlL10 = edt10StijlL.getText().toString();
        String strStijlL20 = edt20StijlL.getText().toString();
        String strStijlL30 = edt30StijlL.getText().toString();
        String strStijlL40 = edt40StijlL.getText().toString();
        String strStijlLDeMontage = edtDeMontageStijlL.getText().toString();

        // ZONE VLEUGEL RV
        String strVleugelRV10 = edt10VleugelRV.getText().toString();
        String strVleugelRV20 = edt20VleugelRV.getText().toString();
        String strVleugelRV30 = edt30VleugelRV.getText().toString();
        String strVleugelRV40 = edt40VleugelRV.getText().toString();
        String strVleugelRVDeMontage = edtDeMontageVleugelRV.getText().toString();

        // ZONE DEUR RV
        String strDeurRV10 = edt10DeurRV.getText().toString();
        String strDeurRV20 = edt20DeurRV.getText().toString();
        String strDeurRV30 = edt30DeurRV.getText().toString();
        String strDeurRV40 = edt40DeurRV.getText().toString();
        String strDeurRVDeMontage = edtDeMontageDeurRV.getText().toString();

        // ZONE DEUR RA
        String strDeurRA10 = edt10DeurRA.getText().toString();
        String strDeurRA20 = edt20DeurRA.getText().toString();
        String strDeurRA30 = edt30DeurRA.getText().toString();
        String strDeurRA40 = edt40DeurRA.getText().toString();
        String strDeurRADeMontage = edtDeMontageDeurRA.getText().toString();

        // ZONE VLEUGEL RA
        String strVleugelRA10 = edt10VleugelRA.getText().toString();
        String strVleugelRA20 = edt20VleugelRA.getText().toString();
        String strVleugelRA30 = edt30VleugelRA.getText().toString();
        String strVleugelRA40 = edt40VleugelRA.getText().toString();
        String strVleugelRADeMontage = edtDeMontageVleugelRA.getText().toString();

        // ZONE A/B/C Stijl R
        String strStijlR10 = edt10StijlR.getText().toString();
        String strStijlR20 = edt20StijlR.getText().toString();
        String strStijlR30 = edt30StijlR.getText().toString();
        String strStijlR40 = edt40StijlR.getText().toString();
        String strStijlRDeMontage = edtDeMontageStijlR.getText().toString();


        if (!strDak10.isEmpty() && !strDak20.isEmpty() && !strDak30.isEmpty() && !strDak40.isEmpty() &&
                !strMo10.isEmpty() && !strMo20.isEmpty() && !strMo30.isEmpty() && !strMo40.isEmpty() &&
                !strKof10.isEmpty() && !strKof20.isEmpty() && !strKof30.isEmpty() && !strKof40.isEmpty() &&
                !strAPaneel10.isEmpty() && !strAPaneel20.isEmpty() && !strAPaneel30.isEmpty() && !strAPaneel40.isEmpty() &&
                !strBumperA10.isEmpty() && !strBumperA20.isEmpty() && !strBumperA30.isEmpty() && !strBumperA40.isEmpty() &&
                !strBumperV10.isEmpty() && !strBumperV20.isEmpty() && !strBumperV30.isEmpty() && !strBumperV40.isEmpty() &&
                !strGrille10.isEmpty() && !strGrille20.isEmpty() && !strGrille30.isEmpty() && !strGrille40.isEmpty() &&
                !strVleugelLV10.isEmpty() && !strVleugelLV20.isEmpty() && !strVleugelLV30.isEmpty() && !strVleugelLV40.isEmpty() &&
                !strDeurLV10.isEmpty() && !strDeurLV20.isEmpty() && !strDeurLV30.isEmpty() && !strDeurLV40.isEmpty() &&
                !strDeurLA10.isEmpty() && !strDeurLA20.isEmpty() && !strDeurLA30.isEmpty() && !strDeurLA40.isEmpty() &&
                !strVleugelLA10.isEmpty() && !strVleugelLA20.isEmpty() && !strVleugelLA30.isEmpty() && !strVleugelLA40.isEmpty() &&
                !strStijlL10.isEmpty() && !strStijlL20.isEmpty() && !strStijlL30.isEmpty() && !strStijlL40.isEmpty() &&
                !strVleugelRV10.isEmpty() && !strVleugelRV20.isEmpty() && !strVleugelRV30.isEmpty() && !strVleugelRV40.isEmpty() &&
                !strDeurRV10.isEmpty() && !strDeurRV20.isEmpty() && !strDeurRV30.isEmpty() && !strDeurRV40.isEmpty() &&
                !strDeurRA10.isEmpty() && !strDeurRA20.isEmpty() && !strDeurRA30.isEmpty() && !strDeurRA40.isEmpty() &&
                !strVleugelRA10.isEmpty() && !strVleugelRA20.isEmpty() && !strVleugelRA30.isEmpty() && !strVleugelRA40.isEmpty() &&
                !strStijlR10.isEmpty() && !strStijlR20.isEmpty() && !strStijlR30.isEmpty() && !strStijlR40.isEmpty()) {

            //ZONE DAK
            int dak10 = Integer.valueOf(strDak10);
            int dak20 = Integer.valueOf(strDak20);
            int dak30 = Integer.valueOf(strDak30);
            int dak40 = Integer.valueOf(strDak40);
            int totalDak = dak10 + dak20 + dak30 + dak40;

            int AEResultDak10 = 0;
            int AEResultDak20 = 0;
            int AEResultDak30 = 0;
            int AEResultDak40 = 0;
            int intDakDemontage = 0;

            //ZONE MOTORKAP
            int mo10 = Integer.valueOf(strMo10);
            int mo20 = Integer.valueOf(strMo20);
            int mo30 = Integer.valueOf(strMo30);
            int mo40 = Integer.valueOf(strMo40);
            int totalMo = mo10 + mo20 + mo30 + mo40;

            int AEResultMo10 = 0;
            int AEResultMo20 = 0;
            int AEResultMo30 = 0;
            int AEResultMo40 = 0;
            int intMoDemontage = 0;

            //ZONE KOFFER/ACHTERKLEP
            int koffer10 = Integer.valueOf(strKof10);
            int koffer20 = Integer.valueOf(strKof20);
            int koffer30 = Integer.valueOf(strKof30);
            int koffer40 = Integer.valueOf(strKof40);
            int totalKoffer = koffer10 + koffer20 + koffer30 + koffer40;

            int AEResultKoffer10 = 0;
            int AEResultKoffer20 = 0;
            int AEResultKoffer30 = 0;
            int AEResultKoffer40 = 0;
            int intKofferDemontage = 0;

            // ZONE ACHTERPANEEL
            int aPaneel10 = Integer.valueOf(strAPaneel10);
            int aPaneel20 = Integer.valueOf(strAPaneel20);
            int aPaneel30 = Integer.valueOf(strAPaneel30);
            int aPaneel40 = Integer.valueOf(strAPaneel40);
            int totalAPaneel = aPaneel10 + aPaneel20 + aPaneel30 + aPaneel40;

            int AEResultAPaneel10 = 0;
            int AEResultAPaneel20 = 0;
            int AEResultAPaneel30 = 0;
            int AEResultAPaneel40 = 0;
            int intAPaneelDemontage = 0;

            // ZONE BUMPER ACHTER
            int bumperA10 = Integer.valueOf(strBumperA10);
            int bumperA20 = Integer.valueOf(strBumperA20);
            int bumperA30 = Integer.valueOf(strBumperA30);
            int bumperA40 = Integer.valueOf(strBumperA40);
            int totalbumperA = bumperA10 + bumperA20 + bumperA30 + bumperA40;

            int AEResultBumperA10 = 0;
            int AEResultBumperA20 = 0;
            int AEResultBumperA30 = 0;
            int AEResultBumperA40 = 0;
            int intBumperADemontage = 0;

            // ZONE BUMPER VOOR
            int bumperV10 = Integer.valueOf(strBumperV10);
            int bumperV20 = Integer.valueOf(strBumperV20);
            int bumperV30 = Integer.valueOf(strBumperV30);
            int bumperV40 = Integer.valueOf(strBumperV40);
            int totalbumperV = bumperV10 + bumperV20 + bumperV30 + bumperV40;

            int AEResultBumperV10 = 0;
            int AEResultBumperV20 = 0;
            int AEResultBumperV30 = 0;
            int AEResultBumperV40 = 0;
            int intBumperVDemontage = 0;

            // ZONE GRILLE
            int grille10 = Integer.valueOf(strGrille10);
            int grille20 = Integer.valueOf(strGrille20);
            int grille30 = Integer.valueOf(strGrille30);
            int grille40 = Integer.valueOf(strGrille40);
            int totalGrille = grille10 + grille20 + grille30 + grille40;

            int AEResultGrille10 = 0;
            int AEResultGrille20 = 0;
            int AEResultGrille30 = 0;
            int AEResultGrille40 = 0;
            int intGrilleDemontage = 0;

            // ZONE VLEUGEL LV
            int vleugelLV10 = Integer.valueOf(strVleugelLV10);
            int vleugelLV20 = Integer.valueOf(strVleugelLV20);
            int vleugelLV30 = Integer.valueOf(strVleugelLV30);
            int vleugelLV40 = Integer.valueOf(strVleugelLV40);
            int totalVleugelLV = vleugelLV10 + vleugelLV20 + vleugelLV30 + vleugelLV40;

            int AEResultVleugelLV10 = 0;
            int AEResultVleugelLV20 = 0;
            int AEResultVleugelLV30 = 0;
            int AEResultVleugelLV40 = 0;
            int intVleugelLVDemontage = 0;

            // ZONE DEUR LV
            int deurLV10 = Integer.valueOf(strDeurLV10);
            int deurLV20 = Integer.valueOf(strDeurLV20);
            int deurLV30 = Integer.valueOf(strDeurLV30);
            int deurLV40 = Integer.valueOf(strDeurLV40);
            int totalDeurLV = deurLV10 + deurLV20 + deurLV30 + deurLV40;

            int AEResultDeurLV10 = 0;
            int AEResultDeurLV20 = 0;
            int AEResultDeurLV30 = 0;
            int AEResultDeurLV40 = 0;
            int intDeurLVDemontage = 0;

            // ZONE DEUR LA
            int deurLA10 = Integer.valueOf(strDeurLA10);
            int deurLA20 = Integer.valueOf(strDeurLA20);
            int deurLA30 = Integer.valueOf(strDeurLA30);
            int deurLA40 = Integer.valueOf(strDeurLA40);
            int totalDeurLA = deurLA10 + deurLA20 + deurLA30 + deurLA40;

            int AEResultDeurLA10 = 0;
            int AEResultDeurLA20 = 0;
            int AEResultDeurLA30 = 0;
            int AEResultDeurLA40 = 0;
            int intDeurLADemontage = 0;

            // ZONE VLEUGEL LA
            int vleugelLA10 = Integer.valueOf(strVleugelLA10);
            int vleugelLA20 = Integer.valueOf(strVleugelLA20);
            int vleugelLA30 = Integer.valueOf(strVleugelLA30);
            int vleugelLA40 = Integer.valueOf(strVleugelLA40);
            int totalVleugelLA = vleugelLA10 + vleugelLA20 + vleugelLA30 + vleugelLA40;

            int AEResultVleugelLA10 = 0;
            int AEResultVleugelLA20 = 0;
            int AEResultVleugelLA30 = 0;
            int AEResultVleugelLA40 = 0;
            int intVleugelLADemontage = 0;

            // ZONE A/B/C Stijl L
            int stijlL10 = Integer.valueOf(strStijlL10);
            int stijlL20 = Integer.valueOf(strStijlL20);
            int stijlL30 = Integer.valueOf(strStijlL30);
            int stijlL40 = Integer.valueOf(strStijlL40);
            int totalStijlL = stijlL10 + stijlL20 + stijlL30 + stijlL40;

            int AEResultStijlL10 = 0;
            int AEResultStijlL20 = 0;
            int AEResultStijlL30 = 0;
            int AEResultStijlL40 = 0;
            int intStijlLDemontage = 0;

            // ZONE VLEUGEL RV
            int vleugelRV10 = Integer.valueOf(strVleugelRV10);
            int vleugelRV20 = Integer.valueOf(strVleugelRV20);
            int vleugelRV30 = Integer.valueOf(strVleugelRV30);
            int vleugelRV40 = Integer.valueOf(strVleugelRV40);
            int totalVleugelRV = vleugelRV10 + vleugelRV20 + vleugelRV30 + vleugelRV40;

            int AEResultVleugelRV10 = 0;
            int AEResultVleugelRV20 = 0;
            int AEResultVleugelRV30 = 0;
            int AEResultVleugelRV40 = 0;
            int intVleugelRVDemontage = 0;

            // ZONE DEUR RV
            int deurRV10 = Integer.valueOf(strDeurRV10);
            int deurRV20 = Integer.valueOf(strDeurRV20);
            int deurRV30 = Integer.valueOf(strDeurRV30);
            int deurRV40 = Integer.valueOf(strDeurRV40);
            int totalDeurRV = deurRV10 + deurRV20 + deurRV30 + deurRV40;

            int AEResultDeurRV10 = 0;
            int AEResultDeurRV20 = 0;
            int AEResultDeurRV30 = 0;
            int AEResultDeurRV40 = 0;
            int intDeurRVDemontage = 0;

            // ZONE DEUR RA
            int deurRA10 = Integer.valueOf(strDeurRA10);
            int deurRA20 = Integer.valueOf(strDeurRA20);
            int deurRA30 = Integer.valueOf(strDeurRA30);
            int deurRA40 = Integer.valueOf(strDeurRA40);
            int totalDeurRA = deurRA10 + deurRA20 + deurRA30 + deurRA40;

            int AEResultDeurRA10 = 0;
            int AEResultDeurRA20 = 0;
            int AEResultDeurRA30 = 0;
            int AEResultDeurRA40 = 0;
            int intDeurRADemontage = 0;

            // ZONE VLEUGEL RA
            int vleugelRA10 = Integer.valueOf(strVleugelRA10);
            int vleugelRA20 = Integer.valueOf(strVleugelRA20);
            int vleugelRA30 = Integer.valueOf(strVleugelRA30);
            int vleugelRA40 = Integer.valueOf(strVleugelRA40);
            int totalVleugelRA = vleugelRA10 + vleugelRA20 + vleugelRA30 + vleugelRA40;

            int AEResultVleugelRA10 = 0;
            int AEResultVleugelRA20 = 0;
            int AEResultVleugelRA30 = 0;
            int AEResultVleugelRA40 = 0;
            int intVleugelRADemontage = 0;

            // ZONE A/B/C Stijl R
            int stijlR10 = Integer.valueOf(strStijlR10);
            int stijlR20 = Integer.valueOf(strStijlR20);
            int stijlR30 = Integer.valueOf(strStijlR30);
            int stijlR40 = Integer.valueOf(strStijlR40);
            int totalStijlR = stijlR10 + stijlR20 + stijlR30 + stijlR40;

            int AEResultStijlR10 = 0;
            int AEResultStijlR20 = 0;
            int AEResultStijlR30 = 0;
            int AEResultStijlR40 = 0;
            int intStijlRDemontage = 0;

            if ((totalDak > 0) || (totalMo > 0) || (totalKoffer > 0) || (totalAPaneel > 0) || (totalbumperA > 0) ||
            (totalbumperV > 10) || (totalGrille > 0) || (totalVleugelLV > 0) || (totalDeurLV > 0) || (totalDeurLA > 0) ||
                    (totalVleugelLA > 0) || (totalStijlL > 0) || (totalVleugelRV > 0) || (totalDeurRV > 0) ||
                    (totalDeurRA > 0) || (totalVleugelRA > 0) || (totalStijlR > 0)) {

                totalDakAE = 0;
                totalMoAE = 0;
                totalKofferAE = 0;
                totalAPaneelAE = 0;
                totalBumperAchterAE = 0;
                totalBumperVoorAE = 0;
                totalGrilleAE = 0;
                totalVleugelLVAE = 0;
                totalDeurLVAE = 0;
                totalDeurLAAE = 0;
                totalVleugelLAAE = 0;
                totalStijlLAE = 0;
                totalVleugelRVAE = 0;
                totalDeurRVAE = 0;
                totalDeurRAAE = 0;
                totalVleugelRAAE = 0;
                totalStijlRAE = 0;

                if (totalDak > 0) {
                    totalDak = totalDak - 1;
                    String resultDak10 = getAE(totalDak, "10");
                    AEResultDak10 = Integer.valueOf(resultDak10);

                    if (dak20 > 0) {
                        dak20 -= 1;
                        String resultDak20 = getAE(dak20, "20");
                        AEResultDak20 = Integer.valueOf(resultDak20);
                    }

                    if (dak30 > 0) {
                        dak30 -= 1;
                        String resultDak30 = getAE(dak30, "30");
                        AEResultDak30 = Integer.valueOf(resultDak30);
                    }

                    if (dak40 > 0) {
                        dak40 -= 1;
                        String resultDak40 = getAE(dak40, "40");
                        AEResultDak40 = Integer.valueOf(resultDak40);
                    }

                    //De/Montage
                    if (!strDakDeMontage.isEmpty()) {
                        intDakDemontage = Integer.valueOf(strDakDeMontage);
                    }

                    totalDakAE = AEResultDak10 + AEResultDak20 + AEResultDak30 + AEResultDak40;

                    //Aluminium + 25 %
                    if (cbAluDak.isChecked()) {
                        if (totalDakAE > 0) {
                            float flAlu = totalDakAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalDakAE = totalDakAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefDak.isChecked()) {
                        if (totalDakAE > 0) {
                            float flKleef = totalDakAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalDakAE = totalDakAE + intResult;
                        }
                    }
                }

                //ZONE MOTORKAP
                if (totalMo > 0) {
                    totalMo = totalMo - 1;
                    String resultMo10 = getAE(totalMo, "10");
                    AEResultMo10 = Integer.valueOf(resultMo10);

                    if (mo20 > 0) {
                        mo20 -= 1;
                        String resultMo20 = getAE(mo20, "20");
                        AEResultMo20 = Integer.valueOf(resultMo20);
                    }

                    if (mo30 > 0) {
                        mo30 -= 1;
                        String resultMo30 = getAE(mo30, "30");
                        AEResultMo30 = Integer.valueOf(resultMo30);
                    }

                    if (mo40 > 0) {
                        mo40 -= 1;
                        String resultMo40 = getAE(mo40, "40");
                        AEResultMo40 = Integer.valueOf(resultMo40);
                    }

                    //De/Montage
                    if (!strMoDeMontage.isEmpty()) {
                        intMoDemontage = Integer.valueOf(strMoDeMontage);
                    }

                    totalMoAE = AEResultMo10 + AEResultMo20 + AEResultMo30 + AEResultMo40;

                    //Aluminium + 25 %
                    if (cbAluMo.isChecked()) {
                        if (totalMoAE > 0) {
                            float flAlu = totalMoAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalMoAE = totalMoAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefMo.isChecked()) {
                        if (totalMoAE > 0) {
                            float flKleef = totalMoAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalMoAE = totalMoAE + intResult;
                        }
                    }
                }

                //ZONE KOFFER/ACHTERKLEP
                if (totalKoffer > 0) {
                    totalKoffer = totalKoffer- 1;
                    String resultKoffer10 = getAE(totalKoffer, "10");
                    AEResultKoffer10 = Integer.valueOf(resultKoffer10);

                    if (koffer20 > 0) {
                        koffer20 -= 1;
                        String resultKoffer20 = getAE(koffer20, "20");
                        AEResultKoffer20 = Integer.valueOf(resultKoffer20);
                    }

                    if (koffer30 > 0) {
                        koffer30 -= 1;
                        String resultKoffer30 = getAE(koffer30, "30");
                        AEResultKoffer30 = Integer.valueOf(resultKoffer30);
                    }

                    if (koffer40 > 0) {
                        koffer40 -= 1;
                        String resultKoffer40 = getAE(koffer40, "40");
                        AEResultKoffer40 = Integer.valueOf(resultKoffer40);
                    }

                    //De/Montage
                    if (!strKofferDeMontage.isEmpty()) {
                        intKofferDemontage = Integer.valueOf(strKofferDeMontage);
                    }

                    totalKofferAE = AEResultKoffer10 + AEResultKoffer20 + AEResultKoffer30 + AEResultKoffer40;

                    //Aluminium + 25 %
                    if (cbAluKoffer.isChecked()) {
                        if (totalKofferAE > 0) {
                            float flAlu = totalKofferAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalKofferAE = totalKofferAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefKoffer.isChecked()) {
                        if (totalKofferAE > 0) {
                            float flKleef = totalKofferAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalKofferAE = totalKofferAE + intResult;
                        }
                    }
                }

                // ZONE ACHTERPANEEL
                if (totalAPaneel > 0) {
                    totalAPaneel = totalAPaneel- 1;
                    String resultAPaneel10 = getAE(totalAPaneel, "10");
                    AEResultAPaneel10 = Integer.valueOf(resultAPaneel10);

                    if (aPaneel20 > 0) {
                        aPaneel20 -= 1;
                        String resultAPaneel20 = getAE(aPaneel20, "20");
                        AEResultAPaneel20 = Integer.valueOf(resultAPaneel20);
                    }

                    if (aPaneel30 > 0) {
                        aPaneel30 -= 1;
                        String resultAPaneel30 = getAE(aPaneel30, "30");
                        AEResultAPaneel30 = Integer.valueOf(resultAPaneel30);
                    }

                    if (aPaneel40 > 0) {
                        aPaneel40 -= 1;
                        String resultAPaneel40 = getAE(aPaneel40, "40");
                        AEResultAPaneel40 = Integer.valueOf(resultAPaneel40);
                    }

                    //De/Montage
                    if (!strAPaneelDeMontage.isEmpty()) {
                        intAPaneelDemontage = Integer.valueOf(strAPaneelDeMontage);
                    }

                    totalAPaneelAE = AEResultAPaneel10 + AEResultAPaneel20 + AEResultAPaneel30 + AEResultAPaneel40;

                    //Aluminium + 25 %
                    if (cbAluAPaneel.isChecked()) {
                        if (totalAPaneelAE > 0) {
                            float flAlu = totalAPaneelAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalAPaneelAE = totalAPaneelAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefAPaneel.isChecked()) {
                        if (totalAPaneelAE > 0) {
                            float flKleef = totalAPaneelAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalAPaneelAE = totalAPaneelAE + intResult;
                        }
                    }
                }

                // Zone Bumper Achter
                if (totalbumperA > 0) {
                    totalbumperA = totalbumperA- 1;
                    String resultBumperA = getAE(totalbumperA, "10");
                    AEResultBumperA10 = Integer.valueOf(resultBumperA);

                    if (bumperA20 > 0) {
                        bumperA20 -= 1;
                        String resultBumperA20 = getAE(bumperA20, "20");
                        AEResultBumperA20= Integer.valueOf(resultBumperA20);
                    }

                    if (bumperA30 > 0) {
                        bumperA30 -= 1;
                        String resultBumperA30 = getAE(bumperA30, "30");
                        AEResultBumperA30 = Integer.valueOf(resultBumperA30);
                    }

                    if (bumperA40 > 0) {
                        bumperA40 -= 1;
                        String resultBumperA40 = getAE(bumperA40, "40");
                        AEResultBumperA40 = Integer.valueOf(resultBumperA40);
                    }

                    //De/Montage
                    if (!strBumperADeMontage.isEmpty()) {
                        intBumperADemontage = Integer.valueOf(strBumperADeMontage);
                    }

                    totalBumperAchterAE = AEResultBumperA10 + AEResultBumperA20 + AEResultBumperA30 + AEResultBumperA40;

                    //Aluminium + 25 %
                    if (cbAluBumperA.isChecked()) {
                        if (totalBumperAchterAE > 0) {
                            float flAlu = totalBumperAchterAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalBumperAchterAE = totalBumperAchterAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefBumperA.isChecked()) {
                        if (totalBumperAchterAE > 0) {
                            float flKleef = totalBumperAchterAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalBumperAchterAE = totalBumperAchterAE + intResult;
                        }
                    }
                }

                // Zone Bumper Voor
                if (totalbumperV > 0) {
                    totalbumperV = totalbumperV - 1;
                    String resultBumperV = getAE(totalbumperV, "10");
                    AEResultBumperV10 = Integer.valueOf(resultBumperV);

                    if (bumperV20 > 0) {
                        bumperV20 -= 1;
                        String resultBumperV20 = getAE(bumperV20, "20");
                        AEResultBumperV20= Integer.valueOf(resultBumperV20);
                    }

                    if (bumperV30 > 0) {
                        bumperV30 -= 1;
                        String resultBumperV30 = getAE(bumperV30, "30");
                        AEResultBumperV30 = Integer.valueOf(resultBumperV30);
                    }

                    if (bumperV40 > 0) {
                        bumperV40 -= 1;
                        String resultBumperV40 = getAE(bumperV40, "40");
                        AEResultBumperV40 = Integer.valueOf(resultBumperV40);
                    }

                    //De/Montage
                    if (!strBumperVDeMontage.isEmpty()) {
                        intBumperVDemontage = Integer.valueOf(strBumperVDeMontage);
                    }

                    totalBumperVoorAE = AEResultBumperV10 + AEResultBumperV20 + AEResultBumperV30 + AEResultBumperV40;

                    //Aluminium + 25 %
                    if (cbAluBumperV.isChecked()) {
                        if (totalBumperVoorAE > 0) {
                            float flAlu = totalBumperVoorAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalBumperVoorAE = totalBumperVoorAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefBumperV.isChecked()) {
                        if (totalBumperVoorAE > 0) {
                            float flKleef = totalBumperVoorAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalBumperVoorAE = totalBumperVoorAE + intResult;
                        }
                    }
                }

                // ZONE GRILLE
                if (totalGrille > 0) {
                    totalGrille = totalGrille - 1;
                    String resultGrille = getAE(totalGrille, "10");
                    AEResultGrille10 = Integer.valueOf(resultGrille);

                    if (grille20 > 0) {
                        grille20 -= 1;
                        String resultGrille20 = getAE(grille20, "20");
                        AEResultGrille20= Integer.valueOf(resultGrille20);
                    }

                    if (grille30 > 0) {
                        grille30 -= 1;
                        String resultGrille30 = getAE(grille30, "30");
                        AEResultGrille30 = Integer.valueOf(resultGrille30);
                    }

                    if (grille40 > 0) {
                        grille40 -= 1;
                        String resultGrille40 = getAE(grille40, "40");
                        AEResultGrille40 = Integer.valueOf(resultGrille40);
                    }

                    //De/Montage
                    if (!strGrilleDeMontage.isEmpty()) {
                        intGrilleDemontage = Integer.valueOf(strGrilleDeMontage);
                    }

                    totalGrilleAE = AEResultGrille10 + AEResultGrille20 + AEResultGrille30 + AEResultGrille40;

                    //Aluminium + 25 %
                    if (cbAluGrille.isChecked()) {
                        if (totalGrilleAE > 0) {
                            float flAlu = totalGrilleAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalGrilleAE = totalGrilleAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefGrille.isChecked()) {
                        if (totalGrilleAE > 0) {
                            float flKleef = totalGrilleAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalGrilleAE = totalGrilleAE + intResult;
                        }
                    }
                }

                // Zone VLEUGEL LV
                if (totalVleugelLV > 0) {
                    totalVleugelLV = totalVleugelLV - 1;
                    String resultVleugelLV = getAE(totalVleugelLV, "10");
                    AEResultVleugelLV10 = Integer.valueOf(resultVleugelLV);

                    if (vleugelLV20 > 0) {
                        vleugelLV20 -= 1;
                        String resultVleugelLV20 = getAE(vleugelLV20, "20");
                        AEResultVleugelLV20 = Integer.valueOf(resultVleugelLV20);
                    }

                    if (vleugelLV30 > 0) {
                        vleugelLV30 -= 1;
                        String resultVleugelLV30 = getAE(vleugelLV30, "30");
                        AEResultVleugelLV30 = Integer.valueOf(resultVleugelLV30);
                    }

                    if (vleugelLV40 > 0) {
                        vleugelLV40 -= 1;
                        String resultVleugelLV40 = getAE(vleugelLV40, "40");
                        AEResultVleugelLV40 = Integer.valueOf(resultVleugelLV40);
                    }

                    //De/Montage
                    if (!strVleugelLVDeMontage.isEmpty()) {
                        intVleugelLVDemontage = Integer.valueOf(strVleugelLVDeMontage);
                    }

                    totalVleugelLVAE = AEResultVleugelLV10 + AEResultVleugelLV20 + AEResultVleugelLV30 + AEResultVleugelLV40;

                    //Aluminium + 25 %
                    if (cbAluVleugelLV.isChecked()) {
                        if (totalVleugelLVAE > 0) {
                            float flAlu = totalVleugelLVAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalVleugelLVAE = totalVleugelLVAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefVleugelLV.isChecked()) {
                        if (totalVleugelLVAE > 0) {
                            float flKleef = totalVleugelLVAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalVleugelLVAE = totalVleugelLVAE + intResult;
                        }
                    }
                }

                // zone DEUR LV
                if (totalDeurLV > 0) {
                    totalDeurLV = totalDeurLV - 1;
                    String resultDeurLV = getAE(totalDeurLV, "10");
                    AEResultDeurLV10 = Integer.valueOf(resultDeurLV);

                    if (deurLV20 > 0) {
                        deurLV20 -= 1;
                        String resultDeurLV20 = getAE(deurLV20, "20");
                        AEResultDeurLV20 = Integer.valueOf(resultDeurLV20);
                    }

                    if (deurLV30 > 0) {
                        deurLV30 -= 1;
                        String resultDeurLV30 = getAE(deurLV30, "30");
                        AEResultDeurLV30 = Integer.valueOf(resultDeurLV30);
                    }

                    if (deurLV40 > 0) {
                        deurLV40 -= 1;
                        String resultDeurLV40 = getAE(deurLV40, "40");
                        AEResultDeurLV40 = Integer.valueOf(resultDeurLV40);
                    }

                    //De/Montage
                    if (!strDeurLVDeMontage.isEmpty()) {
                        intDeurLVDemontage = Integer.valueOf(strDeurLVDeMontage);
                    }

                    totalDeurLVAE = AEResultDeurLV10 + AEResultDeurLV20 + AEResultDeurLV30 + AEResultDeurLV40;

                    //Aluminium + 25 %
                    if (cbAluDeurLV.isChecked()) {
                        if (totalDeurLVAE > 0) {
                            float flAlu = totalDeurLVAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalDeurLVAE = totalDeurLVAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefDeurLV.isChecked()) {
                        if (totalDeurLVAE > 0) {
                            float flKleef = totalDeurLVAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalDeurLVAE = totalDeurLVAE + intResult;
                        }
                    }
                }

                // zone DEUR LA
                if (totalDeurLA > 0) {
                    totalDeurLA = totalDeurLA - 1;
                    String resultDeurLA = getAE(totalDeurLA, "10");
                    AEResultDeurLA10 = Integer.valueOf(resultDeurLA);

                    if (deurLA20 > 0) {
                        deurLA20 -= 1;
                        String resultDeurLA20 = getAE(deurLA20, "20");
                        AEResultDeurLA20 = Integer.valueOf(resultDeurLA20);
                    }

                    if (deurLA30 > 0) {
                        deurLA30 -= 1;
                        String resultDeurLA30 = getAE(deurLA30, "30");
                        AEResultDeurLA30 = Integer.valueOf(resultDeurLA30);
                    }

                    if (deurLA40 > 0) {
                        deurLA40 -= 1;
                        String resultDeurLA40 = getAE(deurLA40, "40");
                        AEResultDeurLA40 = Integer.valueOf(resultDeurLA40);
                    }

                    //De/Montage
                    if (!strDeurLADeMontage.isEmpty()) {
                        intDeurLADemontage = Integer.valueOf(strDeurLADeMontage);
                    }

                    totalDeurLAAE = AEResultDeurLA10 + AEResultDeurLA20 + AEResultDeurLA30 + AEResultDeurLA40;

                    //Aluminium + 25 %
                    if (cbAluDeurLA.isChecked()) {
                        if (totalDeurLAAE > 0) {
                            float flAlu = totalDeurLAAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalDeurLAAE = totalDeurLAAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefDeurLA.isChecked()) {
                        if (totalDeurLAAE > 0) {
                            float flKleef = totalDeurLAAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalDeurLAAE = totalDeurLAAE + intResult;
                        }
                    }
                }

                // Zone VLEUGEL LA
                if (totalVleugelLA > 0) {
                    totalVleugelLA = totalVleugelLA - 1;
                    String resultVleugelLA = getAE(totalVleugelLA, "10");
                    AEResultVleugelLA10 = Integer.valueOf(resultVleugelLA);

                    if (vleugelLA20 > 0) {
                        vleugelLA20 -= 1;
                        String resultVleugelLA20 = getAE(vleugelLA20, "20");
                        AEResultVleugelLA20 = Integer.valueOf(resultVleugelLA20);
                    }

                    if (vleugelLA30 > 0) {
                        vleugelLA30 -= 1;
                        String resultVleugelLA30 = getAE(vleugelLA30, "30");
                        AEResultVleugelLA30 = Integer.valueOf(resultVleugelLA30);
                    }

                    if (vleugelLA40 > 0) {
                        vleugelLA40 -= 1;
                        String resultVleugelLA40 = getAE(vleugelLA40, "40");
                        AEResultVleugelLA40 = Integer.valueOf(resultVleugelLA40);
                    }

                    //De/Montage
                    if (!strVleugelLADeMontage.isEmpty()) {
                        intVleugelLADemontage = Integer.valueOf(strVleugelLADeMontage);
                    }

                    totalVleugelLAAE = AEResultVleugelLA10 + AEResultVleugelLA20 + AEResultVleugelLA30 + AEResultVleugelLA40;

                    //Aluminium + 25 %
                    if (cbAluVleugelLA.isChecked()) {
                        if (totalVleugelLAAE > 0) {
                            float flAlu = totalVleugelLAAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalVleugelLAAE = totalVleugelLAAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefVleugelLA.isChecked()) {
                        if (totalVleugelLAAE > 0) {
                            float flKleef = totalVleugelLAAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalVleugelLAAE = totalVleugelLAAE + intResult;
                        }
                    }
                }

                // Zone A/B/C Stijl L
                if (totalStijlL > 0) {
                    totalStijlL = totalStijlL - 1;
                    String resultStijlL = getAE(totalStijlL, "10");
                    AEResultStijlL10 = Integer.valueOf(resultStijlL);

                    if (stijlL20 > 0) {
                        stijlL20 -= 1;
                        String resultStijlL20 = getAE(stijlL20, "20");
                        AEResultStijlL20 = Integer.valueOf(resultStijlL20);
                    }

                    if (stijlL30 > 0) {
                        stijlL30 -= 1;
                        String resultStijlL30 = getAE(stijlL30, "30");
                        AEResultStijlL30 = Integer.valueOf(resultStijlL30);
                    }

                    if (stijlL40 > 0) {
                        stijlL40 -= 1;
                        String resultStijlL40 = getAE(stijlL40, "40");
                        AEResultStijlL40 = Integer.valueOf(resultStijlL40);
                    }

                    //De/Montage
                    if (!strStijlLDeMontage.isEmpty()) {
                        intStijlLDemontage = Integer.valueOf(strStijlLDeMontage);
                    }

                    totalStijlLAE = AEResultStijlL10 + AEResultStijlL20 + AEResultStijlL30 + AEResultStijlL40;

                    //Aluminium + 25 %
                    if (cbAluStijlL.isChecked()) {
                        if (totalStijlLAE > 0) {
                            float flAlu = totalStijlLAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalStijlLAE = totalStijlLAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefStijlL.isChecked()) {
                        if (totalStijlLAE > 0) {
                            float flKleef = totalStijlLAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalStijlLAE = totalStijlLAE + intResult;
                        }
                    }
                }

                // Zone VLEUGEL RV
                if (totalVleugelRV > 0) {
                    totalVleugelRV = totalVleugelRV - 1;
                    String resultVleugelRV = getAE(totalVleugelRV, "10");
                    AEResultVleugelRV10 = Integer.valueOf(resultVleugelRV);

                    if (vleugelRV20 > 0) {
                        vleugelRV20 -= 1;
                        String resultVleugelRV20 = getAE(vleugelRV20, "20");
                        AEResultVleugelRV20 = Integer.valueOf(resultVleugelRV20);
                    }

                    if (vleugelRV30 > 0) {
                        vleugelRV30 -= 1;
                        String resultVleugelRV30 = getAE(vleugelRV30, "30");
                        AEResultVleugelRV30 = Integer.valueOf(resultVleugelRV30);
                    }

                    if (vleugelRV40 > 0) {
                        vleugelRV40 -= 1;
                        String resultVleugelRV40 = getAE(vleugelRV40, "40");
                        AEResultVleugelRV40 = Integer.valueOf(resultVleugelRV40);
                    }

                    //De/Montage
                    if (!strVleugelRVDeMontage.isEmpty()) {
                        intVleugelRVDemontage = Integer.valueOf(strVleugelRVDeMontage);
                    }

                    totalVleugelRVAE = AEResultVleugelRV10 + AEResultVleugelRV20 + AEResultVleugelRV30 + AEResultVleugelRV40;

                    //Aluminium + 25 %
                    if (cbAluVleugelRV.isChecked()) {
                        if (totalVleugelRVAE > 0) {
                            float flAlu = totalVleugelRVAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalVleugelRVAE = totalVleugelRVAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefVleugelRV.isChecked()) {
                        if (totalVleugelRVAE > 0) {
                            float flKleef = totalVleugelRVAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalVleugelRVAE = totalVleugelRVAE + intResult;
                        }
                    }
                }

                // zone DEUR RV
                if (totalDeurRV > 0) {
                    totalDeurRV = totalDeurRV - 1;
                    String resultDeurRV = getAE(totalDeurRV, "10");
                    AEResultDeurRV10 = Integer.valueOf(resultDeurRV);

                    if (deurRV20 > 0) {
                        deurRV20 -= 1;
                        String resultDeurRV20 = getAE(deurRV20, "20");
                        AEResultDeurRV20 = Integer.valueOf(resultDeurRV20);
                    }

                    if (deurRV30 > 0) {
                        deurRV30 -= 1;
                        String resultDeurRV30 = getAE(deurRV30, "30");
                        AEResultDeurRV30 = Integer.valueOf(resultDeurRV30);
                    }

                    if (deurRV40 > 0) {
                        deurRV40 -= 1;
                        String resultDeurRV40 = getAE(deurRV40, "40");
                        AEResultDeurRV40 = Integer.valueOf(resultDeurRV40);
                    }

                    //De/Montage
                    if (!strDeurRVDeMontage.isEmpty()) {
                        intDeurRVDemontage = Integer.valueOf(strDeurRVDeMontage);
                    }

                    totalDeurRVAE = AEResultDeurRV10 + AEResultDeurRV20 + AEResultDeurRV30 + AEResultDeurRV40;

                    //Aluminium + 25 %
                    if (cbAluDeurRV.isChecked()) {
                        if (totalDeurRVAE > 0) {
                            float flAlu = totalDeurRVAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalDeurRVAE = totalDeurRVAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefDeurRV.isChecked()) {
                        if (totalDeurRVAE > 0) {
                            float flKleef = totalDeurRVAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalDeurRVAE = totalDeurRVAE + intResult;
                        }
                    }
                }

                // zone DEUR RA
                if (totalDeurRA > 0) {
                    totalDeurRA = totalDeurRA- 1;
                    String resultDeurRA = getAE(totalDeurRA, "10");
                    AEResultDeurRA10 = Integer.valueOf(resultDeurRA);

                    if (deurRA20 > 0) {
                        deurRA20 -= 1;
                        String resultDeurRA20 = getAE(deurRA20, "20");
                        AEResultDeurRA20 = Integer.valueOf(resultDeurRA20);
                    }

                    if (deurRA30 > 0) {
                        deurRA30 -= 1;
                        String resultDeurRA30 = getAE(deurRA30, "30");
                        AEResultDeurRA30 = Integer.valueOf(resultDeurRA30);
                    }

                    if (deurRA40 > 0) {
                        deurRA40 -= 1;
                        String resultDeurRA40 = getAE(deurRA40, "40");
                        AEResultDeurRA40 = Integer.valueOf(resultDeurRA40);
                    }

                    //De/Montag
                    if (!strDeurRADeMontage.isEmpty()) {
                        intDeurRADemontage = Integer.valueOf(strDeurRADeMontage);
                    }

                    totalDeurRAAE = AEResultDeurRA10 + AEResultDeurRA20 + AEResultDeurRA30 + AEResultDeurRA40;

                    //Aluminium + 25 %
                    if (cbAluDeurRA.isChecked()) {
                        if (totalDeurRAAE > 0) {
                            float flAlu = totalDeurRAAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalDeurRAAE = totalDeurRAAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefDeurRA.isChecked()) {
                        if (totalDeurRAAE > 0) {
                            float flKleef = totalDeurRAAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalDeurRAAE = totalDeurRAAE + intResult;
                        }
                    }
                }

                // Zone VLEUGEL RA
                if (totalVleugelRA > 0) {
                    totalVleugelRA = totalVleugelRA - 1;
                    String resultVleugelRA = getAE(totalVleugelRA, "10");
                    AEResultVleugelRA10 = Integer.valueOf(resultVleugelRA);

                    if (vleugelRA20 > 0) {
                        vleugelRA20 -= 1;
                        String resultVleugelRA20 = getAE(vleugelRA20, "20");
                        AEResultVleugelRA20 = Integer.valueOf(resultVleugelRA20);
                    }

                    if (vleugelRA30 > 0) {
                        vleugelRA30 -= 1;
                        String resultVleugelRA30 = getAE(vleugelRA30, "30");
                        AEResultVleugelRA30 = Integer.valueOf(resultVleugelRA30);
                    }

                    if (vleugelRA40 > 0) {
                        vleugelRA40 -= 1;
                        String resultVleugelRA40 = getAE(vleugelRA40, "40");
                        AEResultVleugelRA40 = Integer.valueOf(resultVleugelRA40);
                    }

                    //De/Montage
                    if (!strVleugelRADeMontage.isEmpty()) {
                        intVleugelRADemontage = Integer.valueOf(strVleugelRADeMontage);
                    }

                    totalVleugelRAAE = AEResultVleugelRA10 + AEResultVleugelRA20 + AEResultVleugelRA30 + AEResultVleugelRA40;

                    //Aluminium + 25 %
                    if (cbAluVleugelRA.isChecked()) {
                        if (totalVleugelRAAE > 0) {
                            float flAlu = totalVleugelRAAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalVleugelRAAE = totalVleugelRAAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefVleugelRA.isChecked()) {
                        if (totalVleugelRAAE > 0) {
                            float flKleef = totalVleugelRAAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalVleugelRAAE = totalVleugelRAAE + intResult;
                        }
                    }
                }

                // Zone A/B/C Stijl R
                if (totalStijlR > 0) {
                    totalStijlR = totalStijlR - 1;
                    String resultStijlR = getAE(totalStijlR, "10");
                    AEResultStijlR10 = Integer.valueOf(resultStijlR);

                    if (stijlR20 > 0) {
                        stijlR20 -= 1;
                        String resultStijlR20 = getAE(stijlR20, "20");
                        AEResultStijlR20 = Integer.valueOf(resultStijlR20);
                    }

                    if (stijlR30 > 0) {
                        stijlR30 -= 1;
                        String resultStijlR30 = getAE(stijlR30, "30");
                        AEResultStijlR30 = Integer.valueOf(resultStijlR30);
                    }

                    if (stijlR40 > 0) {
                        stijlR40 -= 1;
                        String resultStijlR40 = getAE(stijlR40, "40");
                        AEResultStijlR40 = Integer.valueOf(resultStijlR40);
                    }

                    //De/Montage
                    if (!strStijlRDeMontage.isEmpty()) {
                        intStijlRDemontage = Integer.valueOf(strStijlRDeMontage);
                    }

                    totalStijlRAE = AEResultStijlR10 + AEResultStijlR20 + AEResultStijlR30 + AEResultStijlR40;

                    //Aluminium + 25 %
                    if (cbAluStijlR.isChecked()) {
                        if (totalStijlRAE > 0) {
                            float flAlu = totalStijlRAE * 0.25F;
                            int intResult = (int) flAlu;
                            totalStijlRAE = totalStijlRAE + intResult;
                        }
                    }

                    //Kleef/Lijm  + 15 %
                    if (cbKleefStijlR.isChecked()) {
                        if (totalStijlRAE > 0) {
                            float flKleef = totalStijlRAE * 0.15F;
                            int intResult = (int) flKleef;
                            totalStijlRAE = totalStijlRAE + intResult;
                        }
                    }
                }



                finalAEResult = totalDakAE + totalMoAE + totalKofferAE + totalAPaneelAE + totalBumperAchterAE
                    + totalBumperVoorAE + totalGrilleAE + totalVleugelLVAE + totalDeurLVAE  + totalDeurLAAE
                    + totalVleugelLAAE + totalStijlLAE + totalVleugelRVAE + totalDeurRVAE + totalDeurRAAE
                    + totalVleugelRAAE + totalStijlRAE;

                totalDeMontage = intDakDemontage + intMoDemontage + intKofferDemontage + intAPaneelDemontage + intBumperADemontage
                    + intBumperVDemontage + intGrilleDemontage + intVleugelLVDemontage + intDeurLVDemontage + intDeurLADemontage
                    + intVleugelLADemontage + intStijlLDemontage + intVleugelRVDemontage + intDeurRVDemontage + intDeurRADemontage
                    + intVleugelRADemontage + intStijlRDemontage;

                Toast.makeText(this, "Result = " + finalAEResult + " AE" + "\n" + "De/montage = " + totalDeMontage + "", Toast.LENGTH_LONG).show();

                finish();

            } else {
                Toast.makeText(this, "Er zijn geen beschadigingen", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Gelieve alle gegevens in te vullen", Toast.LENGTH_LONG).show();
        }
    }

    public String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("upexlist.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String getAE(int whichObj, String aantal) {
        try {
            JSONArray array = new JSONArray(readJSONFromAsset());
            JSONObject obj = array.getJSONObject(whichObj);  //element
            String getal = obj.getString(aantal); // veld  "10" of "20" .....
            return getal;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
