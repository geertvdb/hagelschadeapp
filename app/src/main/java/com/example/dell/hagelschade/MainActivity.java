package com.example.dell.hagelschade;

import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.dell.hagelschade.database.DatabaseHandler;
import com.example.dell.hagelschade.http.ApiConnector;
import com.example.dell.hagelschade.model.Bestek;
import com.example.dell.hagelschade.pdf.CreateLocalPdf;
import com.example.dell.hagelschade.pdf.CreatePDFTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.DocumentException;
import com.liuguangqiang.cookie.CookieBar;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ImageButton btnHersteller, btnKlant, btnVoertuig, btnParameters, btnZones;
    private Button btnHersteller2, btnKlant2, btnVoertuig2, btnParameters2, btnZones2;
    private SharedPreferences.Editor editor;
    private String currentDateTime;
    private Bestek bestek;
    private int totaleKostprijs;
    private JSONArray jArray;
    private JSONObject jsonObject;
    private final String url = "http://192.168.115.2/testphpscript.php";
    private InsertHttpDataTask insertHttpDataTask;
    private CreatePDFTask createPDFTask;
    private SQLiteDatabase sqLiteDatabase;
    private DatabaseHandler dbHandler;
    private String folder_path;
    private String image_path;
    private ImageView imageView;
    private String _klant_naam = "";
    private static File myFile; // om pdf document op te halen direct na aanmaak
    private CreateLocalPdf pdf;
    private boolean isPDFCreated = false;
    private boolean isBestekSavedInDB = false;
    private SharedPreferences prefIsSaved;
    private SharedPreferences.Editor editorIsSaved;
    private SharedPreferences.Editor editorUseCamera;
    private String pathPreview;
    private boolean isPicturePreview = false;
    private boolean canStartCamera = false;
    private String tempKlantNaam = "";
    private GlobalVars mApp;
    private boolean isChecked = false;  //menu layout switch


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

        Intent intent = getIntent();
        isChecked = intent.getBooleanExtra("layout",false);
        String klantNaam = intent.getStringExtra("glbStrKlantNaam");
        if(klantNaam != null){
            mApp.setGlbKlantNaam(intent.getStringExtra("glbStrKlantNaam"));
        }else{
            mApp.setGlbKlantNaam("");
        }

        mApp.setGlbCanUseCamera(intent.getBooleanExtra("glbBlnCanUseCamera", false));
        isBestekSavedInDB = intent.getBooleanExtra("isBestekSavedInDB",false);

        //mApp.setGlbKlantNaam("");
        //mApp.setGlbCanUseCamera(false);

        if (savedInstanceState != null) {
            String path = savedInstanceState.getString("Picture");
            Picasso.with(MainActivity.this).load(path).resize(10, 10).centerCrop().into(imageView);

            //Toast.makeText(MainActivity.this, "path: " + path, Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(MainActivity.this, "else blok", Toast.LENGTH_LONG).show();
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "op image geklikt", Toast.LENGTH_LONG).show();
                if (isPicturePreview) {

                    SharedPreferences prefKlant = getApplicationContext().getSharedPreferences("KlantPrefFile", MODE_PRIVATE); // 0 - for private mode
                    //String naamKlant = prefKlant.getString("NaamKlant", "");
                    String naamKlant = _klant_naam;

                    ListActivity listActivity = new ListActivity();
                    ArrayList<File> result = listActivity.getPicturesBestekKlant(naamKlant);

                    if (result == null) {
                        Toast.makeText(getApplicationContext(), "Bestek zonder foto's", Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, ViewPagerActivity.class);
                        intent.putExtra("files", result);
                        intent.putExtra("klantnaam", naamKlant);
                        intent.putExtra("fullscreen", false);
                        startActivity(intent);
                    }
                    /*Intent intent = new Intent(MainActivity.this, PicturePreviewActivity.class);
                    intent.putExtra("PathPreview", pathPreview);
                    startActivity(intent); */
                }
            }
        });

        prefIsSaved = getApplicationContext().getSharedPreferences("KlantNaamPrefFile", MODE_PRIVATE); // 0 - for private mode
        editorIsSaved = prefIsSaved.edit();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {

            } else {
                requestPermission();
            }
        }


        btnHersteller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnHersteller.startAnimation(animation);

                Intent intent = new Intent(MainActivity.this, HerstellerActivity.class);
                startActivity(intent);

            }
        });

        btnKlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnKlant.startAnimation(animation);

                Intent intent = new Intent(MainActivity.this, KlantActivity.class);
                startActivity(intent);
            }
        });

        btnVoertuig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnVoertuig.startAnimation(animation);

                Intent intent = new Intent(MainActivity.this, VoertuigActivity.class);
                startActivity(intent);
            }
        });

        btnParameters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnParameters.startAnimation(animation);

                Intent intent = new Intent(MainActivity.this, ParametersActivity.class);
                startActivity(intent);
            }
        });

        btnZones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnZones.startAnimation(animation);

                Intent intent = new Intent(MainActivity.this, ZoneActivity.class);
                startActivity(intent);
            }
        });

    }

    protected boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    protected void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    private void initialize() {
        pdf = new CreateLocalPdf(MainActivity.this);
        createPDFTask = new CreatePDFTask(MainActivity.this);
        btnHersteller = (ImageButton) findViewById(R.id.btnHersteller);
        btnKlant = (ImageButton) findViewById(R.id.btnKlant);
        btnVoertuig = (ImageButton) findViewById(R.id.btnVoertuig);
        btnParameters = (ImageButton) findViewById(R.id.btnParameters);
        btnZones = (ImageButton) findViewById(R.id.btnZones);
        imageView = (ImageView) findViewById(R.id.ivImageView);
        mApp = (GlobalVars) getApplicationContext();

        dbHandler = new DatabaseHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.rc_nieuw:
                deletePrefs();
                // deletePrefKlantNaam();
                // deletePrefUseCamera();
                mApp.setGlbKlantNaam("");
                mApp.setGlbCanUseCamera(false);
                // canStartCamera = false;
                isPicturePreview = false;
                Picasso.with(MainActivity.this).load(R.drawable.auto).resize(100, 100).centerCrop().into(imageView);
                isBestekSavedInDB = false;
                isPDFCreated = false;
                //Toast.makeText(MainActivity.this, "Nieuw bestek...", Toast.LENGTH_SHORT).show();
                getCookieBarBottom("Nieuw bestek", "Geef alle gegevens in...");
                return true;

            case R.id.rc_list:
                Bestek bestekKlant = getAllBestekData();
                String naamKlant = bestekKlant.getNaamKlant();
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putExtra("isbesteksaved", !isBestekSavedInDB);
                intent.putExtra("naam_klant", naamKlant);
                startActivity(intent);

                return true;

            case R.id.rc_pics:
                openPictureFolder();
                return true;

            case R.id.rc_pdf_map:
                openPDFMap();
                return true;

            case R.id.rc_xls_map:
                openXLSMap();
                return true;
         /*   case R.id.rc_pdf:
                bestek = getAllBestekData();
                String klant_naam = bestek.getNaamKlant();
                if (!klant_naam.equals("")) {
                    jsonObject = convertToJSON(bestek);
                    insertHttpDataTask = new InsertHttpDataTask();
                    insertHttpDataTask.execute(new ApiConnector());
                } else {
                    Toast.makeText(this, "Gelieve eerst een bestek aan te maken...", Toast.LENGTH_LONG).show();
                }
                return true; */

           /* case R.id.rc_pdf_local:
                Bestek bestek = getAllBestekData();
                _klant_naam = bestek.getNaamKlant();
                if (!_klant_naam.equals("")) {
                    if (!isBestekSavedInDB) {
                        saveBestekData(bestek);
                        isBestekSavedInDB = true;
                    }
                    if (!createPDFTask.isCancelled()) {
                        createPDFTask.cancel(true);
                        createPDFTask = null;
                        createPDFTask = new CreatePDFTask(MainActivity.this);
                    }

                    createPDFTask.execute(bestek);
                    Toast.makeText(MainActivity.this, "PDF-document aangemaakt!!", Toast.LENGTH_LONG).show();


                } else {
                    //Toast.makeText(MainActivity.this, "Gelieve eerst een bestek aan te maken...", Toast.LENGTH_SHORT).show();
                    getCookieBarBottom("Gelieve eerst een bestek aan te maken...", "");
                }

                return true; */


            case R.id.rc_camera:
                //Bestek bestekData = getAllBestekData();
              /*  SharedPreferences prefKlantNaam = getApplicationContext().getSharedPreferences("KlantNaamPref", MODE_PRIVATE); // 0 - for private mode
                String klantNaam = prefKlantNaam.getString("NaamKlant","");
                _klant_naam = klantNaam;

                SharedPreferences prefUseCamera = getApplicationContext().getSharedPreferences("UseCameraPref", MODE_PRIVATE); // 0 - for private mode
                boolean useCamera = prefUseCamera.getBoolean("useCamera",false);
                canStartCamera = useCamera;
                Toast.makeText(MainActivity.this, "Klant= " + _klant_naam + " " + "boolean= " + canStartCamera, Toast.LENGTH_LONG).show(); */

                _klant_naam = mApp.getGlbKlantNaam();
                canStartCamera = mApp.isGlbCanUseCamera();
                //  Toast.makeText(MainActivity.this, "Klant= " + _klant_naam + " " + "boolean= " + canStartCamera, Toast.LENGTH_LONG).show();


                if (!_klant_naam.equals("")) {
                    if (canStartCamera) {
                        Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = getFile();
                        camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        //startActivity(camera_intent);
                        startActivityForResult(camera_intent, 100);
                    } else {
                        getCookieBarBottom("Gelieve bestek eerst op te slaan...", "");
                    }

                } else {
                    //Toast.makeText(this, "Gelieve eerst een bestek aan te maken...", Toast.LENGTH_LONG).show();
                    getCookieBarBottom("Gelieve eerst een bestek aan te maken...", "");
                }
                return true;

            case R.id.rc_save:

                Bestek __bestek = getAllBestekData();
                _klant_naam = __bestek.getNaamKlant();
                boolean blnCheckKlantExist = checkIfKlantExists(_klant_naam);
                if (!blnCheckKlantExist) {
                    if (!_klant_naam.equals("") && !isBestekSavedInDB) {
                        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Opslaan")
                                .setMessage("Wil je de gegevens opslaan???")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Bestek _bestek = getAllBestekData();
                                        saveBestekData(_bestek);
                                        isBestekSavedInDB = true;
                                        _klant_naam = _bestek.getNaamKlant();
                                        folder_path = Environment.getExternalStorageDirectory() + "/DCIM/hagelschade/" + _klant_naam;
                                        File folder = new File(folder_path);
                                        if (!folder.exists()) {
                                            folder.mkdir();
                                        }
                                        // canStartCamera = true;
                                   /* SharedPreferences prefUseCamera = getApplicationContext().getSharedPreferences("UseCameraPref", MODE_PRIVATE); // 0 - for private mode
                                    editorUseCamera = prefUseCamera.edit();
                                    editorUseCamera.putBoolean("useCamera", true);
                                    editorUseCamera.commit(); */
                                        mApp.setGlbCanUseCamera(true);
                                        mApp.setGlbKlantNaam(_klant_naam);
                                        deletePrefs();
                                        //Toast.makeText(MainActivity.this, "Data opgeslagen...", Toast.LENGTH_SHORT).show();
                                        getCookieBarTop("Data opgeslagen...", "Bestek klant : " + _klant_naam);

                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //Toast.makeText(MainActivity.this, "Er zijn geen gegevens opgeslagen!!!!", Toast.LENGTH_LONG).show();
                                        getCookieBarTop("Er zijn geen gegevens opgeslagen!", "");
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } else {
                        //Toast.makeText(MainActivity.this, "Gegevens zijn al opgeslagen of maak nieuw bestek...", Toast.LENGTH_SHORT).show();
                        getCookieBarBottom("Gegevens zijn al opgeslagen of nieuw bestek", "");
                    }

                }
                return true;

            case R.id.rc_switch_layout:
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                if (isChecked) {
                    //Toast.makeText(MainActivity.this,"Menu CHECKED", Toast.LENGTH_LONG).show();
                    //setContentView(R.layout.activity_main2);
                    Intent intentLayout = new Intent(MainActivity.this, MainActivity2.class);
                    intentLayout.putExtra("layout", true);
                    intentLayout.putExtra("glbStrKlantNaam", mApp.getGlbKlantNaam());
                    intentLayout.putExtra("glbBlnCanUseCamera", mApp.isGlbCanUseCamera());
                    intentLayout.putExtra("isBestekSavedInDB", isBestekSavedInDB);
                    startActivity(intentLayout);
                    finish();
                } else {
                    //Toast.makeText(MainActivity.this,"Menu UN...CHECKED", Toast.LENGTH_LONG).show();
                    //setContentView(R.layout.activity_main);
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem checkable = menu.findItem(R.id.rc_switch_layout);
        checkable.setChecked(isChecked);
        return true;
    }

    private void openPDFMap() {
        Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/hagelschadePDF");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(selectedUri, "resource/folder");
        startActivity(Intent.createChooser(intent, "Open PDF map"));
    }

    private void openXLSMap() {
        Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/hagelschadeXLS");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(selectedUri, "resource/folder");
        startActivity(Intent.createChooser(intent, "Open XLS map"));
    }


    private File getFile() {

        folder_path = Environment.getExternalStorageDirectory() + "/DCIM/hagelschade/" + _klant_naam;
        File folder = new File(folder_path);
        if (!folder.exists()) {
            folder.mkdir();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        image_path = "IMG_" + timeStamp + ".jpg";
        File image_file = new File(folder, image_path);

        return image_file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                isPicturePreview = true;
                pathPreview = "file://" + folder_path + "/" + image_path;
                //Uri imageContent = data.getData(); //werkt niet
                Picasso.with(MainActivity.this).load(pathPreview).resize(100, 100).centerCrop().into(imageView);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("Picture", "pathPreview");
        super.onSaveInstanceState(outState);

    }

    private Bestek getAllBestekData() {
        Bestek bestek = new Bestek();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("HerstellerPrefFile", MODE_PRIVATE); // 0 - for private mode
        bestek.setNaamHersteller(pref.getString("NaamHersteller", ""));
        bestek.setAdresHersteller(pref.getString("AdresHersteller", ""));
        bestek.setPostcodeHersteller(pref.getString("PostcodeHersteller", ""));
        bestek.setGemeenteHersteller(pref.getString("GemeenteHersteller", ""));
        bestek.setOndernemingsNrHersteller(pref.getString("OndernemingsNrHersteller", ""));

        SharedPreferences prefKlant = getApplicationContext().getSharedPreferences("KlantPrefFile", MODE_PRIVATE); // 0 - for private mode
        bestek.setNaamKlant(prefKlant.getString("NaamKlant", ""));
        bestek.setTelefoonKlant(prefKlant.getString("TelefoonKlant", ""));
        bestek.setEmailKlant(prefKlant.getString("EmailKlant", ""));
        bestek.setVerzekeraarKlant(prefKlant.getString("VerzekeraarKlant", ""));
        bestek.setVerzekerdKlant(prefKlant.getInt("VerzekerdKlant", 0));

        SharedPreferences prefVoertuig = getApplicationContext().getSharedPreferences("VoertuigPrefFile", MODE_PRIVATE); // 0 - for private mode
        bestek.setMerkVoertuig(prefVoertuig.getString("MerkVoertuig", ""));
        bestek.setModelVoertuig(prefVoertuig.getString("ModelVoertuig", ""));
        bestek.setNummerPlVoertuig(prefVoertuig.getString("NummerplaatVoertuig", ""));
        bestek.setChassisNrVoertuig(prefVoertuig.getString("ChassisNrVoertuig", ""));
        bestek.setKleurVoertuig(prefVoertuig.getString("KleurVoertuig", ""));

        SharedPreferences prefParams = getApplicationContext().getSharedPreferences("ParamsPrefFile", MODE_PRIVATE); // 0 - for private mode
        int totalParams = prefParams.getInt("totalParams", 0);
        bestek.setTotalParams(prefParams.getInt("totalParams", 0));
        bestek.setUurloonHagelSchade(prefParams.getInt("UurloonHagelSchade", 0));
        int uurloonHagelSchade = prefParams.getInt("UurloonHagelSchade", 0);
        bestek.setUurloonDemontage(prefParams.getInt("UurloonDeMontage", 0));

        SharedPreferences prefZone = getApplicationContext().getSharedPreferences("ZonePrefFile", MODE_PRIVATE);
        bestek.setTotalAE(prefZone.getInt("TotaalHagelSchade", 0));
        int totalAE = prefZone.getInt("TotaalHagelSchade", 0);
        bestek.setTotalAEDeMontage(prefZone.getInt("TotaalDeMontage", 0));
        int intAEDemontage = prefZone.getInt("TotaalDeMontage", 0);
        int finalAE = totalAE + totalParams + intAEDemontage;

        // totale kostprijs herstelling
        if (uurloonHagelSchade > 0 && finalAE > 0) {
            double temp = uurloonHagelSchade * finalAE / 10;
            //plus BTW
            double btw = temp * 0.21;
            double dbTotalePrijs = temp + btw;

            totaleKostprijs = (int) dbTotalePrijs;
            bestek.setTotaleKostprijs(totaleKostprijs);
        }

        bestek.setCurrentDateTime(getCurrentDateTime());

        return bestek;


    }

    private void deletePrefUseCamera() {
        SharedPreferences prefsUseCamera = getSharedPreferences("UseCameraPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorUseCamera = prefsUseCamera.edit();
        editorUseCamera.clear();
        editorUseCamera.commit();
    }

    private void deletePrefKlantNaam() {
        SharedPreferences prefsKlant = getSharedPreferences("KlantNaamPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorKlant = prefsKlant.edit();
        editorKlant.clear();
        editorKlant.commit();
    }

    private void deletePrefs() {
        SharedPreferences prefsKlant = getSharedPreferences("KlantPrefFile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorKlant = prefsKlant.edit();
        editorKlant.clear();
        editorKlant.commit();

        SharedPreferences prefsVoertuig = getApplicationContext().getSharedPreferences("VoertuigPrefFile", MODE_PRIVATE);
        SharedPreferences.Editor editorVoertuig = prefsVoertuig.edit();
        editorVoertuig.clear();
        editorVoertuig.commit();

        SharedPreferences prefParams = getApplicationContext().getSharedPreferences("ParamsPrefFile", MODE_PRIVATE); // 0 - for private mode
        SharedPreferences.Editor editorParams = prefParams.edit();
        editorParams.clear();
        editorParams.commit();

        SharedPreferences prefZone = getApplicationContext().getSharedPreferences("ZonePrefFile", MODE_PRIVATE); // 0 - for private mode
        SharedPreferences.Editor editorZone = prefZone.edit();
        editorZone.clear();
        editorZone.commit();

    }

    // get current systemtime
    private String getCurrentDateTime() {
        Calendar calender = Calendar.getInstance();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return simpledateformat.format(calender.getTime());

    }

    // convert POJO to JSONObject
    private JSONObject convertToJSON(Bestek bestek) {
        List<Bestek> list = new ArrayList<Bestek>();
        list.add(bestek);
        Gson gson = new Gson();
        Type type = new TypeToken<List<Bestek>>() {
        }.getType();
        String json = gson.toJson(list, type);
        System.out.println(json);
        try {
            jArray = new JSONArray(json);
            jsonObject = jArray.getJSONObject(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Toast.makeText(MainActivity.this, "jsonobject = " + jsonObject + "", Toast.LENGTH_LONG).show();

        return jsonObject;

    }

    // save bestek data in database
    private void saveBestekData(Bestek bestek) {
        sqLiteDatabase = dbHandler.getWritableDatabase();
        dbHandler.addBestekData(bestek, sqLiteDatabase);
    }

    //open picture folder
    private void openPictureFolder() {
        Uri selectedUri = Uri.parse(Environment.getExternalStorageDirectory() + "/DCIM/hagelschade");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(selectedUri, "resource/folder");
        startActivity(Intent.createChooser(intent, "Open foto map"));
    }

    //background thread to send data over the internet
    private class InsertHttpDataTask extends AsyncTask<ApiConnector, Void, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Opslaan... Even geduld...");
            dialog.show();
        }

        @Override
        protected String doInBackground(ApiConnector... params) {
            params[0].sendHttpData(url, jsonObject);
            return "Bestek is toegevoegd";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }

    }

    private void getCookieBarTop(String title, String message) {
        new CookieBar.Builder(MainActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setBackgroundColor(android.R.color.holo_blue_light)
                .setDuration(1000)
                .show();
    }

    private void getCookieBarBottom(String title, String message) {
        new CookieBar.Builder(MainActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setBackgroundColor(android.R.color.holo_blue_light)
                .setLayoutGravity(Gravity.BOTTOM)
                .show();
    }

    private boolean checkIfKlantExists(String klantNaam) {
        boolean match = false;
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "hagelschade");
        String[] files = file.list();
        List<String> lstList = Arrays.asList(files);
        for (int i = 0; i < lstList.size(); i++) {
            String temp = lstList.get(i);
            if (temp.equals(klantNaam)) {
                match = true;
                getCookieBarBottom("OPGELET!!", "Klantnaam bestaat al...");
            }
        }

        return match;

    /*    match = lstList.contains(klantNaam);
        if(match){
            getCookieBarBottom("OPGELET!!", "Klantnaam bestaat al...");
            return match;
        }else{
            return match;
        } */
    }

}
