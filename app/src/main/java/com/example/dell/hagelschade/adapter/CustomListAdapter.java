package com.example.dell.hagelschade.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dell.hagelschade.R;
import com.example.dell.hagelschade.model.Bestek;

import java.util.ArrayList;
import java.util.List;

import static android.R.id.list;

/**
 * Created by Dell on 11/03/2017.
 */

public class CustomListAdapter  extends ArrayAdapter<Bestek>{

    List<Bestek> dataList = new ArrayList<Bestek>();
    Context context;

    public CustomListAdapter(Context context, List<Bestek> dataList) {
        super(context, R.layout.list_item, dataList);
        this.dataList = dataList;
        this.context=context;

    }

    static class LayoutHandler {
        TextView tvNaamKlant;
        TextView tvMerkVoertuig;
        TextView tvDateTime;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutHandler layoutHandler;
        row = convertView;

        if(row == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            row = layoutInflater.inflate(R.layout.list_item, parent, false);
            layoutHandler = new LayoutHandler();
            layoutHandler.tvNaamKlant = (TextView) row.findViewById(R.id.tvItemKlantNaam);
            layoutHandler.tvMerkVoertuig = (TextView) row.findViewById(R.id.tvItemMerkVoertuig);
            layoutHandler.tvDateTime = (TextView) row.findViewById(R.id.tvItemDatum);
            row.setTag(layoutHandler);
        }else {
            layoutHandler = (LayoutHandler) row.getTag();
        }

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        row.startAnimation(animation);
        lastPosition = position;

        Bestek dataProvider = (Bestek) this.getItem(position); // return elk OBJECT van de list
        layoutHandler.tvNaamKlant.setText(dataProvider.getNaamKlant());
        layoutHandler.tvMerkVoertuig.setText(dataProvider.getMerkVoertuig());
        layoutHandler.tvDateTime.setText(dataProvider.getCurrentDateTime());

        return row;
    }
}
