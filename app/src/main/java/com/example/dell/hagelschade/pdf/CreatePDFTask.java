package com.example.dell.hagelschade.pdf;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.dell.hagelschade.ListActivity;
import com.example.dell.hagelschade.MainActivity;
import com.example.dell.hagelschade.model.Bestek;
import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;

/**
 * Created by Dell on 8/05/2017.
 */

public class CreatePDFTask extends AsyncTask<Bestek, Void, File> {

    private ProgressDialog dialog;
    public File myFile;
    private CreateLocalPdf pdf;
    private Context context;
    private OnPostExecuteListener onPostExecuteListener;

    public CreatePDFTask(Context context){
        this.context = context;
        pdf = new CreateLocalPdf(context);

    }

    /**
     * The callback interface
     */
    public interface OnPostExecuteListener {
        void onPostExecute(File file);
    }

    public void setOnPostExecuteListener(OnPostExecuteListener listener){
        this.onPostExecuteListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage("Opslaan... Even geduld...");
        dialog.show();
    }

    @Override
    protected File doInBackground(Bestek... params) {


        try {
            myFile = pdf.createLocalPDF(params[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return myFile;

    }

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);

        if (dialog != null) {
            dialog.dismiss();
            dialog = null;

        }
        if(onPostExecuteListener !=null){
            onPostExecuteListener.onPostExecute(file);
        }
        myFile = file;
        Toast.makeText(context, "PDF-document aangemaakt...", Toast.LENGTH_LONG).show();
        createDialog();
    }

    public File getFolderPathPDF(){
        return myFile;
    }

    private void createDialog(){
        AlertDialog dialog2 = new AlertDialog.Builder(context)
                .setTitle("PDF Document")
                .setMessage("Wil je het PDF document bekijken ?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        try {
                            context.startActivity(intent);
                        }catch(ActivityNotFoundException e){
                            Toast.makeText(context, "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(context, "Er zijn geen gegevens opgeslagen!!!!", Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
