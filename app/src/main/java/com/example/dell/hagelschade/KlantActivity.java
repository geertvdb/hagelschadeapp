package com.example.dell.hagelschade;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

public class KlantActivity extends AppCompatActivity {

    private EditText edtNaamKlant, edtTelefoonKlant, edtEmailKlant, edtVerzekeraarKlant;
    private CheckBox cbVerzekerd;
    private Button btnOpslaanKlant;
    private  SharedPreferences.Editor editor;
    private  GlobalVars mApp;
    private String folder_path;
   // private  SharedPreferences.Editor editorNaamKlant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_klant);

        initialize();

        btnOpslaanKlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnOpslaanKlant.startAnimation(animation);

                String naamKlant        = edtNaamKlant.getText().toString();
                String telefoonKlant    = edtTelefoonKlant.getText().toString();
                String emailKlant       = edtEmailKlant.getText().toString();
                String verzekeraarKlant = edtVerzekeraarKlant.getText().toString();
                int verzekerdKlant;
                if(cbVerzekerd.isChecked()){
                    verzekerdKlant = 1;
                }else{
                    verzekerdKlant = 0;
                }

                editor.putString("NaamKlant", naamKlant);
                editor.putString("TelefoonKlant", telefoonKlant);
                editor.putString("EmailKlant", emailKlant);
                editor.putString("VerzekeraarKlant", verzekeraarKlant);
                editor.putInt("VerzekerdKlant", verzekerdKlant);
                editor.commit();
                mApp.setGlbKlantNaam(naamKlant);
               // editorNaamKlant.putString("NaamKlant", naamKlant);
               // editorNaamKlant.commit();

                finish();
            }
        });
    }

    private void initialize() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("KlantPrefFile", MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();
       // SharedPreferences prefKlantNaam = getApplicationContext().getSharedPreferences("KlantNaamPref", MODE_PRIVATE); // 0 - for private mode
        //editorNaamKlant = prefKlantNaam.edit();

        mApp = (GlobalVars) getApplicationContext();

        edtNaamKlant = (EditText) findViewById(R.id.edtNaamKlant);
        edtTelefoonKlant = (EditText) findViewById(R.id.edtTelefoonKlant);
        edtEmailKlant = (EditText) findViewById(R.id.edtEmailKlant);
        edtVerzekeraarKlant = (EditText) findViewById(R.id.edtVerzekeraarKlant);
        cbVerzekerd = (CheckBox) findViewById(R.id.cbVerzekerd);

        btnOpslaanKlant = (Button) findViewById(R.id.btnOpslaanKlant);

        edtNaamKlant.setText( pref.getString("NaamKlant",""));
        edtTelefoonKlant.setText( pref.getString("TelefoonKlant", ""));
        edtEmailKlant.setText( pref.getString("EmailKlant", ""));
        edtVerzekeraarKlant.setText( pref.getString("VerzekeraarKlant",""));
        int verzekerdKlant = pref.getInt("VerzekerdKlant", 0);
        if(verzekerdKlant == 0){
            cbVerzekerd.setChecked(false);
        }else{
            cbVerzekerd.setChecked(true);
        }
    }
}
