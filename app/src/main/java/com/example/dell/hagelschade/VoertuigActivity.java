package com.example.dell.hagelschade;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;

public class VoertuigActivity extends AppCompatActivity {

    private EditText edtMerk, edtModel, edtNummerPlaat, edtChassisNr, edtKleur;
    private Button btnVoertuig;
    private  SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voertuig);

        initialize();

        btnVoertuig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnVoertuig.startAnimation(animation);

                String merkVoertuig = edtMerk.getText().toString();
                String modelVoertuig = edtModel.getText().toString();
                String nummerPlVoertuig = edtNummerPlaat.getText().toString();
                String chassisNrVoertuig = edtChassisNr.getText().toString();
                String kleurVoertuig = edtKleur.getText().toString();

                editor.putString("MerkVoertuig", merkVoertuig);
                editor.putString("ModelVoertuig", modelVoertuig);
                editor.putString("NummerplaatVoertuig", nummerPlVoertuig);
                editor.putString("ChassisNrVoertuig", chassisNrVoertuig);
                editor.putString("KleurVoertuig", kleurVoertuig);
                editor.commit();

                finish();
            }
        });
    }

    private void initialize() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("VoertuigPrefFile", MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();

        edtMerk = (EditText) findViewById(R.id.edtMerkVoertuig);
        edtModel = (EditText) findViewById(R.id.edtModelVoertuig);
        edtNummerPlaat = (EditText) findViewById(R.id.edtNummerplaatVoertuig);
        edtChassisNr = (EditText) findViewById(R.id.edtChassisNrVoertuig);
        edtKleur = (EditText) findViewById(R.id.edtKleurVoertuig);
        btnVoertuig = (Button) findViewById(R.id.btnOpslaanVoertuig);

        edtMerk.setText( pref.getString("MerkVoertuig",""));
        edtModel.setText( pref.getString("ModelVoertuig", ""));
        edtNummerPlaat.setText( pref.getString("NummerplaatVoertuig", ""));
        edtChassisNr.setText( pref.getString("ChassisNrVoertuig",""));
        edtKleur.setText( pref.getString("KleurVoertuig", ""));
    }
}
