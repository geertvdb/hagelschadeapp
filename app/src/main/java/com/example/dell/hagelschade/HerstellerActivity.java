package com.example.dell.hagelschade;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;

public class HerstellerActivity extends AppCompatActivity {

    private EditText edtNaamHersteller, edtAdresHersteller, edtPostcodeHersteller, edtGemeenteHersteller, edtOndernemingNrHersteller;
    private Button btnHersteller;
    private  SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hersteller);

        initialize();

        btnHersteller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnHersteller.startAnimation(animation);

                String naam = edtNaamHersteller.getText().toString();
                String adres = edtAdresHersteller.getText().toString();
                String postcode = edtPostcodeHersteller.getText().toString();
                String gemeente = edtGemeenteHersteller.getText().toString();
                String ondernemingsNr = edtOndernemingNrHersteller.getText().toString();

                editor.putString("NaamHersteller", naam);
                editor.putString("AdresHersteller", adres);
                editor.putString("PostcodeHersteller", postcode);
                editor.putString("GemeenteHersteller", gemeente);
                editor.putString("OndernemingsNrHersteller", ondernemingsNr);
                editor.commit();

                finish();
            }
        });

    }

    private void initialize() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("HerstellerPrefFile", MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();

        edtNaamHersteller = (EditText) findViewById(R.id.edtNaamHersteller);
        edtAdresHersteller = (EditText) findViewById(R.id.edtAdresHersteller);
        edtPostcodeHersteller = (EditText) findViewById(R.id.edtPostcodeHersteller);
        edtGemeenteHersteller = (EditText) findViewById(R.id.edtGemeenteHersteller);
        edtOndernemingNrHersteller = (EditText) findViewById(R.id.edtOndernemerNrHersteller);

        edtNaamHersteller.setText( pref.getString("NaamHersteller",""));
        edtAdresHersteller.setText( pref.getString("AdresHersteller", ""));
        edtPostcodeHersteller.setText( pref.getString("PostcodeHersteller", ""));
        edtGemeenteHersteller.setText( pref.getString("GemeenteHersteller",""));
        edtOndernemingNrHersteller.setText( pref.getString("OndernemingsNrHersteller", ""));

        btnHersteller = (Button) findViewById(R.id.btnVolgendeHersteller);
    }
}
