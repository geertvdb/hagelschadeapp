package com.example.dell.hagelschade.model;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Dell on 8/03/2017.
 */

public class Bestek {

    int id = 0;
    String naamHersteller;
    String adresHersteller;
    String postcodeHersteller;
    String gemeenteHersteller;
    String ondernemingsNrHersteller;
    String naamKlant;
    String telefoonKlant;
    String emailKlant;
    String verzekeraarKlant;
    int verzekerdKlant;
    String merkVoertuig;
    String modelVoertuig;
    String nummerPlVoertuig;
    String chassisNrVoertuig;
    String kleurVoertuig;
    int totalParams;
    int uurloonHagelSchade;
    int uurloonDemontage;
    int totalAE;
    int totalAEDeMontage;
    String currentDateTime;
    int totaleKostprijs;

    public Bestek(){
    }

    public Bestek(String naamHersteller, String adresHersteller, String postcodeHersteller, String gemeenteHersteller,
                  String ondernemingsNrHersteller, String naamKlant, String telefoonKlant, String emailKlant,
                  String verzekeraarKlant, int verzekerdKlant, String merkVoertuig, String modelVoertuig,
                  String nummerPlVoertuig, String chassisNrVoertuig, String kleurVoertuig, int totalParams,
                  int uurloonHagelSchade, int uurloonDemontage, int totalAE, int totalAEDeMontage,
                  String currentDateTime, int totaleKostprijs) {

        this.naamHersteller = naamHersteller;
        this.adresHersteller = adresHersteller;
        this.postcodeHersteller = postcodeHersteller;
        this.gemeenteHersteller = gemeenteHersteller;
        this.ondernemingsNrHersteller = ondernemingsNrHersteller;
        this.naamKlant = naamKlant;
        this.telefoonKlant = telefoonKlant;
        this.emailKlant = emailKlant;
        this.verzekeraarKlant = verzekeraarKlant;
        this.verzekerdKlant = verzekerdKlant;
        this.merkVoertuig = merkVoertuig;
        this.modelVoertuig = modelVoertuig;
        this.nummerPlVoertuig = nummerPlVoertuig;
        this.chassisNrVoertuig = chassisNrVoertuig;
        this.kleurVoertuig = kleurVoertuig;
        this.totalParams = totalParams;
        this.uurloonHagelSchade = uurloonHagelSchade;
        this.uurloonDemontage = uurloonDemontage;
        this.totalAE = totalAE;
        this.totalAEDeMontage = totalAEDeMontage;
        this.currentDateTime = currentDateTime;
        this.totaleKostprijs = totaleKostprijs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaamHersteller() {
        return naamHersteller;
    }

    public void setNaamHersteller(String naamHersteller) {
        this.naamHersteller = naamHersteller;
    }

    public String getAdresHersteller() {
        return adresHersteller;
    }

    public void setAdresHersteller(String adresHersteller) {
        this.adresHersteller = adresHersteller;
    }

    public String getPostcodeHersteller() {
        return postcodeHersteller;
    }

    public void setPostcodeHersteller(String postcodeHersteller) {
        this.postcodeHersteller = postcodeHersteller;
    }

    public String getGemeenteHersteller() {
        return gemeenteHersteller;
    }

    public void setGemeenteHersteller(String gemeenteHersteller) {
        this.gemeenteHersteller = gemeenteHersteller;
    }

    public String getOndernemingsNrHersteller() {
        return ondernemingsNrHersteller;
    }

    public void setOndernemingsNrHersteller(String ondernemingsNrHersteller) {
        this.ondernemingsNrHersteller = ondernemingsNrHersteller;
    }

    public String getNaamKlant() {
        return naamKlant;
    }

    public void setNaamKlant(String naamKlant) {
        this.naamKlant = naamKlant;
    }

    public String getTelefoonKlant() {
        return telefoonKlant;
    }

    public void setTelefoonKlant(String telefoonKlant) {
        this.telefoonKlant = telefoonKlant;
    }

    public String getEmailKlant() {
        return emailKlant;
    }

    public void setEmailKlant(String emailKlant) {
        this.emailKlant = emailKlant;
    }

    public String getVerzekeraarKlant() {
        return verzekeraarKlant;
    }

    public void setVerzekeraarKlant(String verzekeraarKlant) {
        this.verzekeraarKlant = verzekeraarKlant;
    }

    public int getVerzekerdKlant() {
        return verzekerdKlant;
    }

    public void setVerzekerdKlant(int verzekerdKlant) {
        this.verzekerdKlant = verzekerdKlant;
    }

    public String getMerkVoertuig() {
        return merkVoertuig;
    }

    public void setMerkVoertuig(String merkVoertuig) {
        this.merkVoertuig = merkVoertuig;
    }

    public String getModelVoertuig() {
        return modelVoertuig;
    }

    public void setModelVoertuig(String modelVoertuig) {
        this.modelVoertuig = modelVoertuig;
    }

    public String getNummerPlVoertuig() {
        return nummerPlVoertuig;
    }

    public void setNummerPlVoertuig(String nummerPlVoertuig) {
        this.nummerPlVoertuig = nummerPlVoertuig;
    }

    public String getChassisNrVoertuig() {
        return chassisNrVoertuig;
    }

    public void setChassisNrVoertuig(String chassisNrVoertuig) {
        this.chassisNrVoertuig = chassisNrVoertuig;
    }

    public String getKleurVoertuig() {
        return kleurVoertuig;
    }

    public void setKleurVoertuig(String kleurVoertuig) {
        this.kleurVoertuig = kleurVoertuig;
    }

    public int getTotalParams() {
        return totalParams;
    }

    public void setTotalParams(int totalParams) {
        this.totalParams = totalParams;
    }

    public int getUurloonHagelSchade() {
        return uurloonHagelSchade;
    }

    public void setUurloonHagelSchade(int uurloonHagelSchade) {
        this.uurloonHagelSchade = uurloonHagelSchade;
    }

    public int getUurloonDemontage() {
        return uurloonDemontage;
    }

    public void setUurloonDemontage(int uurloonDemontage) {
        this.uurloonDemontage = uurloonDemontage;
    }

    public int getTotalAE() {
        return totalAE;
    }

    public void setTotalAE(int totalAE) {
        this.totalAE = totalAE;
    }

    public int getTotalAEDeMontage() {
        return totalAEDeMontage;
    }

    public void setTotalAEDeMontage(int totalAEDeMontage) {
        this.totalAEDeMontage = totalAEDeMontage;
    }

    public String getCurrentDateTime() {
        return currentDateTime;
    }

    public void setCurrentDateTime(String currentDateTime) {
        this.currentDateTime = currentDateTime;
    }

    public int getTotaleKostprijs() {
        return totaleKostprijs;
    }

    public void setTotaleKostprijs(int totaleKostprijs) {
        this.totaleKostprijs = totaleKostprijs;
    }
}
