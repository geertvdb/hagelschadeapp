package com.example.dell.hagelschade.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.dell.hagelschade.ListActivity;
import com.example.dell.hagelschade.model.Bestek;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 10/03/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "bestekDB";

    // Table name
    private static final String TABLE_BESTEK = "bestek";

    // Table Columns names
    private static final String KEY_BESTEK_ID = "bestek_id";
    private static final String KEY_NAAM_HERSTELLER = "naamhersteller";
    private static final String KEY_ADRES_HERSTELLER = "adreshersteller";
    private static final String KEY_POSTCODE_HERSTELLER = "postcodehersteller";
    private static final String KEY_GEMEENTE_HERSTELLER = "gemeentehersteller";
    private static final String KEY_ONDERNEMINGSNR_HERSTELLER = "ondernemingsnrhersteller";
    private static final String KEY_NAAM_KLANT = "naamklant";
    private static final String KEY_TELEFOON_KLANT = "telefoonklant";
    private static final String KEY_EMAIL_KLANT = "emailklant";
    private static final String KEY_VERZEKERAAR_KLANT = "verzekeraarklant";
    private static final String KEY_VERZEKERD_KLANT = "verzekerdklant";
    private static final String KEY_MERK_VOERTUIG = "merkvoertuig";
    private static final String KEY_MODEL_VOERTUIG = "modelvoertuig";
    private static final String KEY_NUMMERPL_VOERTUIG = "nummerplvoertuig";
    private static final String KEY_CHASSISNR_VOERTUIG = "chassisnrvoertuig";
    private static final String KEY_KLEUR_VOERTUIG = "kleurvoertuig";
    private static final String KEY_TOTAL_PARAMS = "totalparams";
    private static final String KEY_UURLOON_HAGELSCHADE = "uurloonhagelschade";
    private static final String KEY_UURLOON_DEMONTAGE = "uurloondemontage";
    private static final String KEY_TOTAL_AE = "totalae";
    private static final String KEY_TOTAL_AE_DEMONTAGE = "totalaedemontage";
    private static final String KEY_DATE_TIME = "datetime";
    private static final String KEY_TOTALE_KOSTPRIJS = "totaleprijs";

    public static final String CREATE_QUERY =
            "CREATE TABLE " + TABLE_BESTEK + "(" + KEY_BESTEK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAAM_HERSTELLER + " TEXT," + KEY_ADRES_HERSTELLER + " TEXT," +
                    KEY_POSTCODE_HERSTELLER + " TEXT," + KEY_GEMEENTE_HERSTELLER + " TEXT," + KEY_ONDERNEMINGSNR_HERSTELLER + " TEXT," +
                    KEY_NAAM_KLANT + " TEXT," + KEY_TELEFOON_KLANT + " TEXT," + KEY_EMAIL_KLANT + " TEXT," + KEY_VERZEKERAAR_KLANT + " TEXT," + KEY_VERZEKERD_KLANT + " INTEGER DEFAULT 0," +
                    KEY_MERK_VOERTUIG + " TEXT," + KEY_MODEL_VOERTUIG + " TEXT," + KEY_NUMMERPL_VOERTUIG + " TEXT," + KEY_CHASSISNR_VOERTUIG + " TEXT," +
                    KEY_KLEUR_VOERTUIG + " TEXT," + KEY_TOTAL_PARAMS + " INTEGER," + KEY_UURLOON_HAGELSCHADE + " INTEGER," +
                    KEY_UURLOON_DEMONTAGE + " INTEGER," + KEY_TOTAL_AE + " INTEGER," + KEY_TOTAL_AE_DEMONTAGE + " INTEGER," +
                    KEY_DATE_TIME + " TEXT," + KEY_TOTALE_KOSTPRIJS + " INTEGER" + ")";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BESTEK);
        // Create table again
        onCreate(db);
    }

    public void addBestekData(Bestek bestek, SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAAM_HERSTELLER, bestek.getNaamHersteller());
        contentValues.put(KEY_ADRES_HERSTELLER, bestek.getAdresHersteller());
        contentValues.put(KEY_POSTCODE_HERSTELLER, bestek.getPostcodeHersteller());
        contentValues.put(KEY_GEMEENTE_HERSTELLER, bestek.getGemeenteHersteller());
        contentValues.put(KEY_ONDERNEMINGSNR_HERSTELLER, bestek.getOndernemingsNrHersteller());
        contentValues.put(KEY_NAAM_KLANT, bestek.getNaamKlant());
        contentValues.put(KEY_TELEFOON_KLANT, bestek.getTelefoonKlant());
        contentValues.put(KEY_EMAIL_KLANT, bestek.getEmailKlant());
        contentValues.put(KEY_VERZEKERAAR_KLANT, bestek.getVerzekeraarKlant());
        contentValues.put(KEY_VERZEKERD_KLANT, bestek.getVerzekerdKlant());
        contentValues.put(KEY_MERK_VOERTUIG, bestek.getMerkVoertuig());
        contentValues.put(KEY_MODEL_VOERTUIG, bestek.getModelVoertuig());
        contentValues.put(KEY_NUMMERPL_VOERTUIG, bestek.getNummerPlVoertuig());
        contentValues.put(KEY_CHASSISNR_VOERTUIG, bestek.getChassisNrVoertuig());
        contentValues.put(KEY_KLEUR_VOERTUIG, bestek.getKleurVoertuig());
        contentValues.put(KEY_TOTAL_PARAMS, bestek.getTotalParams());
        contentValues.put(KEY_UURLOON_HAGELSCHADE, bestek.getUurloonHagelSchade());
        contentValues.put(KEY_UURLOON_DEMONTAGE, bestek.getUurloonDemontage());
        contentValues.put(KEY_TOTAL_AE, bestek.getTotalAE());
        contentValues.put(KEY_TOTAL_AE_DEMONTAGE, bestek.getTotalAEDeMontage());
        contentValues.put(KEY_DATE_TIME, bestek.getCurrentDateTime());
        contentValues.put(KEY_TOTALE_KOSTPRIJS, bestek.getTotaleKostprijs());


        db.insert(TABLE_BESTEK, null, contentValues); // null argument is nodig
        //voor als content value is leeg er geen lege rij met null values wordt toegevoegd
        // minstens 1 nullcolom name toevoegen
        Log.e("DATABASE_OPERATIONS", "One row inserted...");
    }

    public Cursor getAllBestekData(SQLiteDatabase db) {
        Cursor cursor;

        //cursor = db.rawQuery("SELECT naamklant, merkvoertuig, datetime FROM " + TABLE_BESTEK + " ORDER BY datetime DESC", null);
        cursor = db.rawQuery("SELECT naamklant, merkvoertuig, datetime FROM " + TABLE_BESTEK, null);

        return cursor;
    }

    public List<Bestek> getBestek(SQLiteDatabase db) {
        Cursor cursor;
        Bestek bestek = null;
        List<Bestek> bestekken = new ArrayList<>();
        String[] projections = {KEY_BESTEK_ID, KEY_NAAM_HERSTELLER, KEY_ADRES_HERSTELLER, KEY_POSTCODE_HERSTELLER, KEY_GEMEENTE_HERSTELLER,
                KEY_ONDERNEMINGSNR_HERSTELLER, KEY_NAAM_KLANT, KEY_TELEFOON_KLANT, KEY_EMAIL_KLANT, KEY_VERZEKERAAR_KLANT,
                KEY_VERZEKERD_KLANT, KEY_MERK_VOERTUIG, KEY_MODEL_VOERTUIG, KEY_NUMMERPL_VOERTUIG, KEY_CHASSISNR_VOERTUIG,
                KEY_KLEUR_VOERTUIG, KEY_TOTAL_PARAMS, KEY_UURLOON_HAGELSCHADE, KEY_UURLOON_DEMONTAGE, KEY_TOTAL_AE,
                KEY_TOTAL_AE_DEMONTAGE, KEY_DATE_TIME, KEY_TOTALE_KOSTPRIJS};

        cursor = db.query(TABLE_BESTEK, projections, null, null, null, null, null);

       if (cursor != null ) {
           if(cursor.moveToFirst()){
               do{
                   bestek = new Bestek();
                   bestek.setId(cursor.getInt(cursor.getColumnIndex("bestek_id")));
                   bestek.setId(cursor.getInt(0));
                   bestek.setNaamHersteller(cursor.getString(cursor.getColumnIndex("naamhersteller")));
                   bestek.setAdresHersteller(cursor.getString(cursor.getColumnIndex("adreshersteller")));
                   bestek.setPostcodeHersteller(cursor.getString(cursor.getColumnIndex("postcodehersteller")));
                   bestek.setGemeenteHersteller(cursor.getString(cursor.getColumnIndex("gemeentehersteller")));
                   bestek.setOndernemingsNrHersteller(cursor.getString(cursor.getColumnIndex("ondernemingsnrhersteller")));

                   bestek.setNaamKlant(cursor.getString(cursor.getColumnIndex("naamklant")));
                   bestek.setTelefoonKlant(cursor.getString(cursor.getColumnIndex("telefoonklant")));
                   bestek.setEmailKlant(cursor.getString(cursor.getColumnIndex("emailklant")));
                   bestek.setVerzekeraarKlant(cursor.getString(cursor.getColumnIndex("verzekeraarklant")));
                   bestek.setVerzekerdKlant(cursor.getInt(cursor.getColumnIndex("verzekerdklant")));

                   bestek.setMerkVoertuig(cursor.getString(cursor.getColumnIndex("merkvoertuig")));
                   bestek.setModelVoertuig(cursor.getString(cursor.getColumnIndex("modelvoertuig")));
                   bestek.setNummerPlVoertuig(cursor.getString(cursor.getColumnIndex("nummerplvoertuig")));
                   bestek.setChassisNrVoertuig(cursor.getString(cursor.getColumnIndex("chassisnrvoertuig")));
                   bestek.setKleurVoertuig(cursor.getString(cursor.getColumnIndex("kleurvoertuig")));

                   bestek.setTotalParams(cursor.getInt(cursor.getColumnIndex("totalparams")));
                   bestek.setUurloonHagelSchade(cursor.getInt(cursor.getColumnIndex("uurloonhagelschade")));
                   bestek.setUurloonDemontage(cursor.getInt(cursor.getColumnIndex("uurloondemontage")));
                   bestek.setTotalAE(cursor.getInt(cursor.getColumnIndex("totalae")));
                   bestek.setTotalAEDeMontage(cursor.getInt(cursor.getColumnIndex("totalaedemontage")));
                   bestek.setTotaleKostprijs(cursor.getInt(cursor.getColumnIndex("totaleprijs")));
                   //bestek.setTotaleKostprijs(cursor.getInt(20));
                   bestek.setCurrentDateTime(cursor.getString(cursor.getColumnIndex("datetime")));

                   bestekken.add(bestek);

               }while (cursor.moveToNext());
           }
           cursor.close();
        }

    return bestekken;
}

    public void deleteBestek(String id, SQLiteDatabase db) {
        String selection = KEY_BESTEK_ID + " LIKE ?";  //where condition
        String[] selection_arg = {id};
        db.delete(TABLE_BESTEK, selection, selection_arg);
    }

    public int updateBestek(String id, Bestek bestek, SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAAM_HERSTELLER, bestek.getNaamHersteller());
        contentValues.put(KEY_ADRES_HERSTELLER, bestek.getAdresHersteller());
        contentValues.put(KEY_POSTCODE_HERSTELLER, bestek.getPostcodeHersteller());
        contentValues.put(KEY_GEMEENTE_HERSTELLER, bestek.getGemeenteHersteller());
        contentValues.put(KEY_ONDERNEMINGSNR_HERSTELLER, bestek.getOndernemingsNrHersteller());
        contentValues.put(KEY_NAAM_KLANT, bestek.getNaamKlant());
        contentValues.put(KEY_TELEFOON_KLANT, bestek.getTelefoonKlant());
        contentValues.put(KEY_EMAIL_KLANT, bestek.getEmailKlant());
        contentValues.put(KEY_VERZEKERAAR_KLANT, bestek.getVerzekeraarKlant());
        contentValues.put(KEY_VERZEKERD_KLANT, bestek.getVerzekerdKlant());
        contentValues.put(KEY_MERK_VOERTUIG, bestek.getMerkVoertuig());
        contentValues.put(KEY_MODEL_VOERTUIG, bestek.getModelVoertuig());
        contentValues.put(KEY_NUMMERPL_VOERTUIG, bestek.getNummerPlVoertuig());
        contentValues.put(KEY_CHASSISNR_VOERTUIG, bestek.getChassisNrVoertuig());
        contentValues.put(KEY_KLEUR_VOERTUIG, bestek.getKleurVoertuig());
        contentValues.put(KEY_TOTAL_PARAMS, bestek.getTotalParams());
        contentValues.put(KEY_UURLOON_HAGELSCHADE, bestek.getUurloonHagelSchade());
        contentValues.put(KEY_UURLOON_DEMONTAGE, bestek.getUurloonDemontage());
        contentValues.put(KEY_TOTAL_AE, bestek.getTotalAE());
        contentValues.put(KEY_TOTAL_AE_DEMONTAGE, bestek.getTotalAEDeMontage());
        contentValues.put(KEY_DATE_TIME, bestek.getCurrentDateTime());
        contentValues.put(KEY_TOTALE_KOSTPRIJS, bestek.getTotaleKostprijs());
        String selection = KEY_BESTEK_ID + " LIKE ?"; //where condition
        String[] selection_args = {id};
        int count = db.update(TABLE_BESTEK, contentValues, selection, selection_args);
        return count;
    }
}
