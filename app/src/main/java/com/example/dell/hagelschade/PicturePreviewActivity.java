package com.example.dell.hagelschade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicturePreviewActivity extends AppCompatActivity {

    private Button btnSluiten;
    private ImageView imageViewPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_preview);

        Intent intent = getIntent();
        String picturePath = intent.getStringExtra("PathPreview");

        imageViewPreview = (ImageView) findViewById(R.id.ivImage_preview);
        btnSluiten = (Button) findViewById(R.id.btnClosePreview);
        //Picasso.with(PicturePreviewActivity.this).load(picturePath).resize(900,700).centerCrop().into(imageViewPreview);
        Picasso.with(PicturePreviewActivity.this).load(picturePath).fit().centerInside().into(imageViewPreview);

        btnSluiten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                btnSluiten.startAnimation(animation);
                finish();
            }
        });

    }
}
