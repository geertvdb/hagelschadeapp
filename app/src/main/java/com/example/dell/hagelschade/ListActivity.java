package com.example.dell.hagelschade;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.internal.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.dell.hagelschade.adapter.CustomListAdapter;
import com.example.dell.hagelschade.adapter.CustomListWithThumbsAdapter;
import com.example.dell.hagelschade.database.DatabaseHandler;
import com.example.dell.hagelschade.http.ApiConnector;
import com.example.dell.hagelschade.model.Bestek;
import com.example.dell.hagelschade.pdf.CreateLocalPdf;
import com.example.dell.hagelschade.pdf.CreatePDFTask;
import com.example.dell.hagelschade.xls.CreateXLS;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.DocumentException;
import com.itextpdf.xmp.impl.Utils;
import com.liuguangqiang.cookie.CookieBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class ListActivity extends AppCompatActivity implements CreatePDFTask.OnPostExecuteListener {

    private List<Bestek> dataBestek;
    private ListView listView;
    private CustomListAdapter adapter;
    private CustomListWithThumbsAdapter adapterThumbs;
    private SQLiteDatabase sqLiteDatabase;
    private DatabaseHandler dbHandler;
    private String selectedItem = "";
    private JSONArray jArray;
    private JSONObject jsonObject;
    private final String url = "http://192.168.115.2/testphpscript.php";
    private InsertHttpPdfTask insertHttpPdfTask;
    private CreatePDFTask createPDFTask;
    private static final int REQUEST_CODE = 1;
    private static final int REQUEST_CODE_PICTURE_SHARE = 2;
    private ShareActionProvider mShareActionProvider;
    private MenuItem item;
    private File myPDFFile;
    private String emailKlantNaam;
    private String emailKlantEmail;
    private String emailHerstellerNaam;
    private String emailAdresHersteller;
    private String emailPostcodeHersteller;
    private String emailGemeentsteHersteller;
    private String emailOndernemersNrHersteller;
    private CreateXLS createXLS = new CreateXLS(this);
    private boolean isXLS = false;
    private boolean isPDF = false;
    private File XLSpath;
    private ArrayList<Uri> urisFotos;
    private Switch mySwitch;
    private Switch switchs = null;
    private  GlobalVars mApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listView = (ListView) findViewById(R.id.lvListView);
        createPDFTask = new CreatePDFTask(ListActivity.this);
        mApp = (GlobalVars) getApplicationContext();
        if(mApp.isBlnSwitch()){
            //switchs.setChecked(true);
            fillList();
        }else{
            //switchs.setChecked(false);
            fillListWithThumbs();
        }
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if(intent != null){
            boolean isBestekSaved = intent.getBooleanExtra("isbesteksaved",false);
            String naam_klant = intent.getStringExtra("naam_klant");
            if(isBestekSaved && !naam_klant.equals("")){
                getCookieBarTop("OPGELET!!!", "Bestek nog niet opgeslagen...");
            }
        }

        // niet meer gebruikt, komt van Listactivity
      /*  Intent intent = getIntent();
        if(intent != null){
            ArrayList<String> list = intent.getStringArrayListExtra("picturefilesforshare");

            if(list != null) {
                urisFotos = new ArrayList<Uri>();
                //convert from paths to Android friendly Parcelable Uri's
                for (String file : list) {
                    File fileIn = new File(file);
                    Uri u = Uri.fromFile(fileIn);
                    urisFotos.add(u);
                }

                for(int i = 0; i < urisFotos.size(); i++){
                    Log.d("RESULTFINAL", "" + urisFotos.get(i));
                }
            }
        } */

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                sqLiteDatabase = dbHandler.getReadableDatabase();
                List<Bestek> bestekken = dbHandler.getBestek(sqLiteDatabase);
                Bestek bestekRowData = bestekken.get(i);

                int bestek_id = bestekRowData.getId();
                String naamHersteller = bestekRowData.getNaamHersteller();
                String adresHersteller = bestekRowData.getAdresHersteller();
                String postcodeHersteller = bestekRowData.getPostcodeHersteller();
                String gemeenteHersteller = bestekRowData.getGemeenteHersteller();
                String ondernemingsNrHersteller = bestekRowData.getOndernemingsNrHersteller();
                String naamKlant = bestekRowData.getNaamKlant();
                String telefoonKlant = bestekRowData.getTelefoonKlant();
                String emailKlant = bestekRowData.getEmailKlant();
                String verzekeraarKlant = bestekRowData.getVerzekeraarKlant();
                int verzekerdKlant = bestekRowData.getVerzekerdKlant();

                String merkVoertuig = bestekRowData.getMerkVoertuig();
                String modelVoertuig = bestekRowData.getModelVoertuig();
                String nummerPlVoertuig = bestekRowData.getNummerPlVoertuig();
                String chassisNrVoertuig = bestekRowData.getChassisNrVoertuig();
                String kleurVoertuig = bestekRowData.getKleurVoertuig();

                int totalParams = bestekRowData.getTotalParams();
                int uurloonHagelschade = bestekRowData.getUurloonHagelSchade();
                int uurloonDeMontage = bestekRowData.getUurloonDemontage();
                int totalAE = bestekRowData.getTotalAE();
                int totalAEDeMontage = bestekRowData.getTotalAEDeMontage();
                String currentDateTime = bestekRowData.getCurrentDateTime();
                int totaleKostprijs = bestekRowData.getTotaleKostprijs();

                Intent intent = new Intent(ListActivity.this, BestekActivity.class);
                intent.putExtra("bestek_id", bestek_id);
                intent.putExtra("naamHersteller", naamHersteller);
                intent.putExtra("adresHersteller", adresHersteller);
                intent.putExtra("postcodeHersteller", postcodeHersteller);
                intent.putExtra("gemeenteHersteller", gemeenteHersteller);
                intent.putExtra("ondernemingsNrHersteller", ondernemingsNrHersteller);
                intent.putExtra("naamKlant", naamKlant);
                intent.putExtra("telefoonKlant", telefoonKlant);
                intent.putExtra("emailKlant", emailKlant);
                intent.putExtra("verzekeraarKlant", verzekeraarKlant);
                intent.putExtra("verzekerdKlant", verzekerdKlant);
                intent.putExtra("merkVoertuig", merkVoertuig);
                intent.putExtra("modelVoertuig", modelVoertuig);
                intent.putExtra("nummerPlVoertuig", nummerPlVoertuig);
                intent.putExtra("chassisNrVoertuig", chassisNrVoertuig);
                intent.putExtra("kleurVoertuig", kleurVoertuig);
                intent.putExtra("totalParams", totalParams);
                intent.putExtra("uurloonHagelschade", uurloonHagelschade);
                intent.putExtra("uurloonDeMontage", uurloonDeMontage);
                intent.putExtra("totalAE", totalAE);
                intent.putExtra("totalAEDeMontage", totalAEDeMontage);
                intent.putExtra("currentDateTime", currentDateTime);
                intent.putExtra("totaleKostprijs", totaleKostprijs);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                getSingleChoiceDialog(i);
                //setShareIntent(createShareIntent());
                // item.setVisible(true);
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.action_share, menu);

        item = menu.findItem(R.id.action_share);
        item.setVisible(false);

        MenuItem item = menu.findItem(R.id.list_with_thumbs);
        item.setActionView(R.layout.show_protected_switch);

        MenuItem toggleservice = menu.findItem(R.id.list_with_thumbs);
        RelativeLayout actionView = (RelativeLayout) toggleservice.getActionView();
      //  actionView.
       //
        switchs = (Switch)actionView.findViewById(R.id.switch_show_protected);
        if(mApp.isBlnSwitch()){
            switchs.setChecked(true);
        }else{
            switchs.setChecked(false);
        }

        //switchs.setChecked(true);
        switchs.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    mApp.setBlnSwitch(true);
                    //Toast.makeText(ListActivity.this, "ON", Toast.LENGTH_LONG).show();
                    fillListWithThumbs();
                }else{
                    mApp.setBlnSwitch(false);
                    //Toast.makeText(ListActivity.this, "OFF", Toast.LENGTH_LONG).show();
                    fillList();
                }
            }
        });



     /*   MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item); // get the spinner
        spinner.setAdapter(adapter);
        //s.setOnItemSelectedListener(onItemSelectedListener);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }); */

     /*
        // Locate MenuItem with ShareActionProvider
        item = menu.findItem(R.id.action_share);
        // Fetch and store ShareActionProvider
        // mShareActionProvider = (ShareActionProvider) item.getActionProvider();
        item.setVisible(false);

        mShareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(item); */

        //setShareIntent(createShareIntent());


        // Return true to display menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_share:
                if (myPDFFile != null && isPDF) {
                    Intent shareIntent;
                    Uri uri = Uri.fromFile(myPDFFile);
                    shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("application/pdf");
                    shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailKlantEmail});
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Bestek Hagelschade!");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Beste " + emailKlantNaam + "\n\nIn bijlage vindt u het bestek van de hagelschade.\n\nMet vriendelijke groeten\n\nDe hersteller" +
                            "\n\nNaam: " + emailHerstellerNaam + "\nAdres: " + emailAdresHersteller + "\nPostcode: " + emailPostcodeHersteller + "\nGemeente: " + emailGemeentsteHersteller + "\nOndernemingsnummer: " + emailOndernemersNrHersteller);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(shareIntent, "Select"));
                } else if (XLSpath != null && isXLS) {
                    Intent shareIntent;
                    Uri uri = Uri.fromFile(XLSpath);
                    shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("application/vnd.ms-excel");
                    shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailKlantEmail});
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Bestek Hagelschade!");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Beste " + emailKlantNaam + "\n\nIn bijlage vindt u het bestek van de hagelschade.\n\nMet vriendelijke groeten\n\nDe hersteller" +
                            "\n\nNaam: " + emailHerstellerNaam + "\nAdres: " + emailAdresHersteller + "\nPostcode: " + emailPostcodeHersteller + "\nGemeente: " + emailGemeentsteHersteller + "\nOndernemingsnummer: " + emailOndernemersNrHersteller);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(shareIntent, "Select"));
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //USED FOR SHARED ACTION PROVIDER
    // Call to update the share intent
  /*  private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    private Intent createShareIntent() {
       /* Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                "http://stackandroid.com");
        return shareIntent; */

    //Uri uri = Uri.parse("file:///storage/emulated/0/hagelschadePDF/Henk  van loveren_20170517_183455.pdf");

    //File outputFile = new File(Environment.getExternalStoragePublicDirectory
    // (Environment.DIRECTORY_DOWNLOADS), "example.pdf");

      /*   File test = myPDFFile;
        Intent shareIntent = null;
       if(myPDFFile != null) {
            Uri uri = Uri.fromFile(myPDFFile);
            shareIntent = new Intent(Intent.ACTION_SEND);
            //shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("application/pdf");
            //shareIntent.setDataAndType(uri, "application/pdf");
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);


          /* Intent intent = new Intent(Intent.ACTION_VIEW);
           intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
           intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);


        }
        return  shareIntent;

    } */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE && data != null) {
            String naam_klant = data.getStringExtra("NaamKlantBestek");
            String updateOrDelete = data.getStringExtra("updateOrDelete");
            String update = "update";
            String delete = "delete";

            if (updateOrDelete.equals(update)) {
                getCookieBarTop("Bestek updated", "Klant: " + naam_klant);
            } else if (updateOrDelete.equals(delete)) {
                getCookieBarTop("Bestek deleted", "Klant: " + naam_klant);
            }
        }

    }

    private void getCookieBarTop(String title, String message) {
        new CookieBar.Builder(ListActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setBackgroundColor(android.R.color.holo_blue_light)
                .setDuration(2000) // 3 seconden
                .show();
    }

    private void getCookieBarBottom(String title, String message) {
        new CookieBar.Builder(ListActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setBackgroundColor(android.R.color.holo_blue_light)
                .setLayoutGravity(Gravity.BOTTOM)
                .show();
    }

    private Cursor getListData() {
        sqLiteDatabase = dbHandler.getWritableDatabase();
        Cursor c = dbHandler.getAllBestekData(sqLiteDatabase);
        return c;
    }

    public void getSingleChoiceDialog(final int j) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);

        // Set the alert dialog title
        builder.setTitle("Kies output...");

        final CharSequence[] output = new String[]{
                "Print PDF", "Print XLS", "Bestek Foto's"
        };

        // Set a single choice items list for alert dialog
        builder.setSingleChoiceItems(output, // Items list
                -1, // Index of checked item (-1 = no selection)
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Get the alert dialog selected item's text
                        switch (i) {
                            case 0:
                                selectedItem = (String) output[i];
                                break;
                            case 1:
                                selectedItem = (String) output[i];
                                break;
                            case 2:
                                selectedItem = (String) output[i];
                                break;
                        }
                    }
                });

        // Set the a;ert dialog positive button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (selectedItem.equals("")) {
                    Toast.makeText(getApplicationContext(), "Er is geen opdracht uitgevoerd...", Toast.LENGTH_LONG).show();
                } else if (selectedItem.equals("Print PDF")) {  // moet zelde naam zijn als charsequence output
                    List<Bestek> bestekken = dbHandler.getBestek(sqLiteDatabase);
                    Bestek bestekRowData = bestekken.get(j);
                    emailKlantNaam = bestekRowData.getNaamKlant();
                    emailKlantEmail = bestekRowData.getEmailKlant();
                    emailHerstellerNaam = bestekRowData.getNaamHersteller();
                    emailAdresHersteller = bestekRowData.getAdresHersteller();
                    emailPostcodeHersteller = bestekRowData.getPostcodeHersteller();
                    emailGemeentsteHersteller = bestekRowData.getGemeenteHersteller();
                    emailOndernemersNrHersteller = bestekRowData.getOndernemingsNrHersteller();
                    if (!createPDFTask.isCancelled()) { // do this because you can use only one instance of asynctask
                        createPDFTask.cancel(true);
                        createPDFTask = null;
                        createPDFTask = new CreatePDFTask(ListActivity.this);
                        createPDFTask.setOnPostExecuteListener(ListActivity.this);
                    }
                    createPDFTask.execute(bestekRowData);
                    item.setVisible(true);
                    selectedItem = "";
                    isPDF = true;
                    isXLS = false;

                } else if (selectedItem.equals("Print PDF(Internet)")) {

                    //not in use for the moment
                    List<Bestek> bestekken = dbHandler.getBestek(sqLiteDatabase);
                    Bestek bestekRowData = bestekken.get(j);
                    jsonObject = convertToJSON(bestekRowData);
                    insertHttpPdfTask = new InsertHttpPdfTask();
                    insertHttpPdfTask.execute(new ApiConnector());

                    Toast.makeText(getApplicationContext(), "PDF-document aangemaakt!!", Toast.LENGTH_LONG).show();
                    selectedItem = "";
                } else if (selectedItem.equals("Print XLS")) {
                    List<Bestek> bestekken = dbHandler.getBestek(sqLiteDatabase);
                    Bestek bestekRowData = bestekken.get(j);
                    emailKlantNaam = bestekRowData.getNaamKlant();
                    emailKlantEmail = bestekRowData.getEmailKlant();
                    emailHerstellerNaam = bestekRowData.getNaamHersteller();
                    emailAdresHersteller = bestekRowData.getAdresHersteller();
                    emailPostcodeHersteller = bestekRowData.getPostcodeHersteller();
                    emailGemeentsteHersteller = bestekRowData.getGemeenteHersteller();
                    emailOndernemersNrHersteller = bestekRowData.getOndernemingsNrHersteller();
                    String klantNaam = bestekRowData.getNaamKlant();
                    jsonObject = convertToJSON(bestekRowData);

                    XLSpath = createXLS.createXLS(bestekRowData, "test");
                    isXLS = true;
                    isPDF = false;
                    item.setVisible(true);
                    createXLSDialog(XLSpath);
                    selectedItem = "";

                } else if (selectedItem.equals("Bestek Foto's")) {
                    List<Bestek> bestekken = dbHandler.getBestek(sqLiteDatabase);
                    Bestek bestekRowData = bestekken.get(j);
                    String klantNaam = bestekRowData.getNaamKlant();
                    String emailKlant = bestekRowData.getEmailKlant();
                    String naamHersteller = bestekRowData.getNaamHersteller();
                    String adresHersteller = bestekRowData.getAdresHersteller();
                    String postcodeHersteller = bestekRowData.getPostcodeHersteller();
                    String gemeenteHersteller = bestekRowData.getGemeenteHersteller();
                    String ondernemensNrHersteller = bestekRowData.getOndernemingsNrHersteller();

                    // Toast.makeText(getApplicationContext(), "Bestek fotoooooos", Toast.LENGTH_LONG).show();
                    ArrayList<File> result = getPicturesBestekKlant(klantNaam);
                    if(result.size() == 0){
                        Toast.makeText(getApplicationContext(), "Bestek zonder foto's", Toast.LENGTH_LONG).show();
                    }else{
                        Intent intent = new Intent(ListActivity.this, ViewPagerActivity.class);
                        intent.putExtra("files", result);
                        intent.putExtra("klantnaam", klantNaam);
                        intent.putExtra("klantemail", emailKlant);
                        intent.putExtra("herstellernaam", naamHersteller);
                        intent.putExtra("herstelleradres", adresHersteller);
                        intent.putExtra("herstellerpostcode", postcodeHersteller);
                        intent.putExtra("herstellergemeente", gemeenteHersteller);
                        intent.putExtra("ondernemernrhersteller", ondernemensNrHersteller);
                        intent.putExtra("fullscreen", true);
                        selectedItem = "";
                        startActivityForResult(intent, REQUEST_CODE_PICTURE_SHARE);
                    }


                    //String[] resultArrayString = result.toString();
                }

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    // convert POJO to JSONObject
    private JSONObject convertToJSON(Bestek bestek) {
        List<Bestek> list = new ArrayList<Bestek>();
        list.add(bestek);
        Gson gson = new Gson();
        Type type = new TypeToken<List<Bestek>>() {
        }.getType();
        String json = gson.toJson(list, type);
        System.out.println(json);
        try {
            jArray = new JSONArray(json);
            jsonObject = jArray.getJSONObject(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

    @Override
    public void onPostExecute(File file) {
        myPDFFile = file;
        //myPDFFile = new File("/storage/emulated/0/hagelschadePDF/Henk  van loveren_20170517_183455.pdf");
    }

    //background thread
    private class InsertHttpPdfTask extends AsyncTask<ApiConnector, Void, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ListActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Opslaan... Even geduld...");
            dialog.show();
        }

        @Override
        protected String doInBackground(ApiConnector... params) {
            params[0].sendHttpData(url, jsonObject);
            return "Bestek is toegevoegd";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }
    }

    private void fillList() {
        dataBestek = new ArrayList<>();
        dbHandler = new DatabaseHandler(this);
        Cursor c = getListData();

        if (c != null) {
            if (c.moveToFirst() && c.getCount() > 0) {
                do {
                    Bestek bestek = new Bestek();
                    bestek.setMerkVoertuig(c.getString(c.getColumnIndex("merkvoertuig")));
                    bestek.setNaamKlant(c.getString(c.getColumnIndex("naamklant")));
                    bestek.setCurrentDateTime(c.getString(c.getColumnIndex("datetime")));

                    dataBestek.add(bestek);

                } while (c.moveToNext());
                c.close();
            }

        }
        adapter = new CustomListAdapter(this, dataBestek);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    private void fillListWithThumbs(){
        dataBestek = new ArrayList<>();
        dbHandler = new DatabaseHandler(this);
        ArrayList<ArrayList<File>> allBestekPics = new ArrayList<>();
        Cursor c = getListData();


        if (c != null) {
            if (c.moveToFirst() && c.getCount() > 0) {
                do {
                    Bestek bestek = new Bestek();
                    bestek.setMerkVoertuig(c.getString(c.getColumnIndex("merkvoertuig")));
                    bestek.setNaamKlant(c.getString(c.getColumnIndex("naamklant")));
                    bestek.setCurrentDateTime(c.getString(c.getColumnIndex("datetime")));
                    ArrayList<File> result = getPicturesBestekKlant(bestek.getNaamKlant());



                    dataBestek.add(bestek);
                    allBestekPics.add(result);

                } while (c.moveToNext());
                c.close();
            }

        }
        adapterThumbs = new CustomListWithThumbsAdapter(this, dataBestek, allBestekPics);
        adapterThumbs.notifyDataSetChanged();
        listView.setAdapter(adapterThumbs);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mApp.isBlnSwitch()){
            fillListWithThumbs();
        }else{
            fillList();
        }
    }

    private void createXLSDialog(final File path) {
        AlertDialog dialog2 = new AlertDialog.Builder(this)
                .setTitle("XLS Document")
                .setMessage("Wil je het XLS document bekijken ?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(path), "application/vnd.ms-excel");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(ListActivity.this, "No Application Available to View Excel", Toast.LENGTH_SHORT).show();
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(context, "Er zijn geen gegevens opgeslagen!!!!", Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public ArrayList<File> getPicturesBestekKlant(String klantNaam) {
        //   /storage/emulated/0/DCIM/hagelschade
        ArrayList<File> klantenFileList = new ArrayList<>();
        ArrayList<String> klantenStringList = new ArrayList<>();
        File m_path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "hagelschade");
        //File[] dirs = m_path.listFiles();
        String[] dirs = m_path.list();
        ArrayList<String> klantenList = new ArrayList<>();
        for (int i = 0; i < dirs.length; i++) {
            klantenList.add(dirs[i].toString());
        }

        String searchString = klantNaam;
        String imageBestekKlant = "";
        for (String klant : klantenList) {
            Log.d("KLANT", klant);
            //if (klant.contains(searchString)) {
                if (klant.matches(searchString)) {
                imageBestekKlant = klant;
                    String test = "/storage/emulated/0/DCIM/hagelschade/" + imageBestekKlant ;
                    File file = new File(test);
                    File[] pics = file.listFiles();
                    if(pics == null){
                        return null;
                    }else{
                        for (int i = 0; i < pics.length; i++) {
                            Log.d("RESULT", pics[i].toString());
                            klantenFileList.add(pics[i]);
                        }
                    }
                    return klantenFileList;
            }
        }

        return null;

    }

}
