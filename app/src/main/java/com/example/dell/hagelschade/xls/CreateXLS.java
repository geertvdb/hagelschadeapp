package com.example.dell.hagelschade.xls;

import android.content.Context;
import android.os.Environment;

import com.example.dell.hagelschade.model.Bestek;

import java.io.File;
import  java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

//import org.apache.poi.*;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Created by Dell on 6/06/2017.
 */

public class CreateXLS {

    private Context context;
    private String naamKlant;
    private String telefoonKlant;
    private String emailKlant;
    private String verzekeraarKlant;
    private String merkVoertuig;
    private String modelVoertuig;
    private String nummerPlaatVoertuig;
    private String chassisNmrVoertuig ;
    private String kleurVoertuig;
    private String totaalHagelschade;
    private String totaalDeMontage;
    private String totaalParams;

    private String finalAE;
    private String totalBedrag;

    public CreateXLS(Context context) {
        this.context = context;
    }

    public File createXLS(Bestek bestek, String fileName){

        naamKlant =  bestek.getNaamKlant();
        telefoonKlant = bestek.getTelefoonKlant();
        emailKlant = bestek.getEmailKlant();
        verzekeraarKlant = bestek.getVerzekeraarKlant();
        merkVoertuig = bestek.getMerkVoertuig();
        modelVoertuig = bestek.getModelVoertuig();
        nummerPlaatVoertuig = bestek.getNummerPlVoertuig();
        chassisNmrVoertuig = bestek.getChassisNrVoertuig();
        kleurVoertuig = bestek.getKleurVoertuig();
        totaalHagelschade = String.valueOf(bestek.getTotalAE());
        int totaalHagelschade = bestek.getTotalAE();
        totaalDeMontage = String.valueOf(bestek.getTotalAEDeMontage());
        int totaalDeMontage = bestek.getTotalAEDeMontage();
        totaalParams = String.valueOf(bestek.getTotalParams());
        int totaalParams = bestek.getTotalParams();
        finalAE  = String.valueOf(totaalHagelschade + totaalDeMontage + totaalParams);
        totalBedrag = String.valueOf(bestek.getTotaleKostprijs());
        finalAE  = String.valueOf(totaalHagelschade + totaalDeMontage + totaalParams);

        Date date = new Date();
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(date);

        String klantNaam = bestek.getNaamKlant();

        File ourPDFFolder = new File(Environment.getExternalStorageDirectory(), "hagelschadeXLS");
        File myFile = new File(ourPDFFolder, klantNaam + "_" + timeStamp + ".xls");

        if(!ourPDFFolder.exists()){
            ourPDFFolder.mkdirs();
        }
        HSSFWorkbook workbook=new HSSFWorkbook();
        HSSFSheet sheet =  workbook.createSheet("Hagelschade");
        HSSFRow rowhead=   sheet.createRow((short)0);
        rowhead.createCell((short) 0).setCellValue("BESTEK HAGELSCHADE");

        HSSFRow row=   sheet.createRow((short)1);
        row.createCell((short) 0).setCellValue("------------------------------------");

        HSSFRow row2 =   sheet.createRow((short)2);
        row2.createCell((short) 0).setCellValue("");

        HSSFRow row3 =   sheet.createRow((short)3);
        row3.createCell((short) 0).setCellValue("KLANT");
        row3.createCell((short) 4).setCellValue("VOERTUIG");

        HSSFRow row4 =   sheet.createRow((short)4);
        row4.createCell((short) 0).setCellValue("----------");
        row4.createCell((short) 4).setCellValue("----------------");

        HSSFRow row5 =   sheet.createRow((short)5);
        row5.createCell((short) 0).setCellValue("Naam: " + bestek.getNaamKlant());
        row5.createCell((short) 4).setCellValue("Merk: " + bestek.getMerkVoertuig());

        HSSFRow row6 =   sheet.createRow((short)6);
        row6.createCell((short) 0).setCellValue("Telefoon: " + bestek.getTelefoonKlant());
        row6.createCell((short) 4).setCellValue("Model: " + bestek.getModelVoertuig());

        HSSFRow row7 =   sheet.createRow((short)7);
        row7.createCell((short) 0).setCellValue("Email: " + bestek.getEmailKlant());
        row7.createCell((short) 4).setCellValue("Nummerplaat: " + bestek.getNummerPlVoertuig());

        HSSFRow row8 =   sheet.createRow((short)8);
        row8.createCell((short) 0).setCellValue("Verzekeraar: " + bestek.getVerzekeraarKlant());
        row8.createCell((short) 4).setCellValue("ChassisNr.: " + bestek.getChassisNrVoertuig());

        HSSFRow row9 =   sheet.createRow((short)9);
        row9.createCell((short) 4).setCellValue("Kleur: " + bestek.getKleurVoertuig());

        HSSFRow row10 =   sheet.createRow((short)10);
        row10.createCell((short) 0).setCellValue("____________________________________________________________");

        HSSFRow row11 =   sheet.createRow((short)11);
        row11.createCell((short) 0).setCellValue("");

        HSSFRow row12 =   sheet.createRow((short)12);
        row12.createCell((short) 0).setCellValue("TOTAAL HAGELSCHADE: ");
        row12.createCell((short) 6).setCellValue(bestek.getTotalAE() + " AE");

        HSSFRow row13 =   sheet.createRow((short)13);
        row13.createCell((short) 0).setCellValue("TOTAAL DE/MONTAGE: ");
        row13.createCell((short) 6).setCellValue(bestek.getTotalAEDeMontage() + " AE");

        HSSFRow row14 =   sheet.createRow((short)14);
        row14.createCell((short) 0).setCellValue("TOTAAL PARAMETERS: ");
        row14.createCell((short) 6).setCellValue(bestek.getTotalParams() + " AE");

        HSSFRow row15 =   sheet.createRow((short)15);
        row15.createCell((short) 0).setCellValue("____________________________________________________________");

        HSSFRow row16 =   sheet.createRow((short)16);
        row16.createCell((short) 0).setCellValue("TOTAAL AE: ");
        row16.createCell((short) 6).setCellValue(finalAE + " AE");

        HSSFRow row17 =   sheet.createRow((short)17);
        row17.createCell((short) 0).setCellValue("");

        HSSFRow row18 =   sheet.createRow((short)18);
        row18.createCell((short) 0).setCellValue("");

        HSSFRow row19 =   sheet.createRow((short)19);
        row19.createCell((short) 0).setCellValue("____________________________________________________________");

        HSSFRow row20 =   sheet.createRow((short)20);
        row20.createCell((short) 0).setCellValue("TOTAAL BEDRAG INCL. BTW: ");
        row20.createCell((short) 6).setCellValue(bestek.getTotaleKostprijs() + " €");

        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(myFile);
            workbook.write(fileOut);
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return myFile;

    }
}
