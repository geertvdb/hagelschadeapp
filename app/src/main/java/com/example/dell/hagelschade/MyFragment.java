package com.example.dell.hagelschade;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Dell on 9/06/2017.
 */

public class MyFragment extends Fragment implements ViewPagerActivity.GetKlantName{

    private TextView textView;
    private String klantNaam;
    //private static View view;
    private CheckBox checkBox;

    public MyFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_vehicles, container, false);

        textView = (TextView) view.findViewById(R.id.tvPagerText);
        textView.setText(klantNaam);
        //checkBox = (CheckBox) view.findViewById(R.id.cbSharePicture);

        return view;
    }

    @Override
    public void getKlantName(String name) {
        this.klantNaam = name;
    }
}

 /*   private static View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.map, container, false);
        } catch (InflateException e) {
     // map is already there, just return view as it is
        }
        return view;
    } */
