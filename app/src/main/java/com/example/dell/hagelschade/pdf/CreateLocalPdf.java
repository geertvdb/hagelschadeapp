package com.example.dell.hagelschade.pdf;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;

import com.example.dell.hagelschade.R;
import com.example.dell.hagelschade.model.Bestek;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dell on 5/05/2017.
 */

public class CreateLocalPdf {

    private File myFile;
    private Document document;
    private String naamKlant;
    private String telefoonKlant;
    private String emailKlant;
    private String verzekeraarKlant;
    private String merkVoertuig;
    private String modelVoertuig;
    private String nummerPlaatVoertuig;
    private String chassisNmrVoertuig ;
    private String kleurVoertuig;
    private String totaalHagelschade;
    private String totaalDeMontage;
    private String totaalParams;

    private String finalAE;
    private String totalBedrag;

    private Context context;

    public CreateLocalPdf(Context context) {
        this.context = context;
    }

    public File createLocalPDF(Bestek bestek) throws IOException, DocumentException {

        naamKlant =  bestek.getNaamKlant();
        telefoonKlant = bestek.getTelefoonKlant();
        emailKlant = bestek.getEmailKlant();
        verzekeraarKlant = bestek.getVerzekeraarKlant();
        merkVoertuig = bestek.getMerkVoertuig();
        modelVoertuig = bestek.getModelVoertuig();
        nummerPlaatVoertuig = bestek.getNummerPlVoertuig();
        chassisNmrVoertuig = bestek.getChassisNrVoertuig();
        kleurVoertuig = bestek.getKleurVoertuig();
        totaalHagelschade = String.valueOf(bestek.getTotalAE());
        int totaalHagelschade = bestek.getTotalAE();
        totaalDeMontage = String.valueOf(bestek.getTotalAEDeMontage());
        int totaalDeMontage = bestek.getTotalAEDeMontage();
        totaalParams = String.valueOf(bestek.getTotalParams());
        int totaalParams = bestek.getTotalParams();
        finalAE  = String.valueOf(totaalHagelschade + totaalDeMontage + totaalParams);
        totalBedrag = String.valueOf(bestek.getTotaleKostprijs());


        Date date = new Date();
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date);

        File ourPDFFolder = new File(Environment.getExternalStorageDirectory(), "hagelschadePDF");
        myFile = new File(ourPDFFolder, naamKlant + "_" + timeStamp + ".pdf");

        if(!ourPDFFolder.exists()){
            ourPDFFolder.mkdirs();
        }

        OutputStream output = new FileOutputStream(myFile);

        //step 1 create document
        document = new Document();
        PdfWriter.getInstance(document, output);
        document.open();

        // different fonts
        Font f = new Font(Font.FontFamily.HELVETICA, 24, Font.UNDERLINE, GrayColor.BLACK);
        Font f2 = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, GrayColor.BLACK);
        Font f3 = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, GrayColor.BLACK);

        // add image
        Drawable d = context.getResources().getDrawable(R.drawable.planmanager);
        BitmapDrawable bitDw = ((BitmapDrawable) d);
        Bitmap bmp = bitDw.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());
        image.scaleAbsolute(206, 75);  // resize picture
        document.add(image);

        // timestamp
        String timeStamp2 = new SimpleDateFormat("HH:mm:ss  dd-MM-yyyy").format(date);
        Paragraph paragraph1 = new Paragraph("Afdruk: " + timeStamp2);
        paragraph1.setAlignment(Element.ALIGN_RIGHT);
        document.add(paragraph1);

        document.add(new Paragraph(" "));
        document.add(new Paragraph("Bestek Hagelschade",f));
        document.add(new Paragraph(" "));
        document.add(new Paragraph(" "));
        document.add(createTable());  // create table
        document.add(new Paragraph(" "));

        Paragraph preface = new Paragraph("Totaal hagelschade:", f2);
        Chunk glue = new Chunk(new VerticalPositionMark());
        preface.add(new Chunk(glue)); //                          Chunk is used for 2 values on same row
        //preface.setAlignment(Element.ALIGN_CENTER);
        preface.add(new Chunk(totaalHagelschade + " AE", f2));  // waarde van totaal hagelschade
        document.add(preface);

        Paragraph preface2 = new Paragraph("Totaal de/montage:", f2);
        Chunk glue2 = new Chunk(new VerticalPositionMark());
        preface2.add(new Chunk(glue2));
        preface2.add(new Chunk(totaalDeMontage + " AE", f2));
        document.add(preface2);

        Paragraph preface3 = new Paragraph("Totaal parameters:", f2);
        Chunk glue3 = new Chunk(new VerticalPositionMark());
        preface3.add(new Chunk(glue3));
        preface3.add(new Chunk(totaalParams + " AE", f2));
        document.add(preface3);

        Chunk line = new Chunk(new LineSeparator());
        document.add(line);

        Paragraph preface4 = new Paragraph("Totaal AE:", f2);
        Chunk glue4 = new Chunk(new VerticalPositionMark());
        preface4.add(new Chunk(glue4));
        preface4.add(new Chunk(finalAE + " AE", f2));
        document.add(preface4);
        document.add(new Paragraph(" "));
        document.add(new Paragraph(" "));
        document.add(line);

        Paragraph preface5 = new Paragraph("Totaal bedrag incl. BTW:", f3);
        Chunk glue5 = new Chunk(new VerticalPositionMark());
        preface5.add(new Chunk(glue5));
        preface5.add(new Chunk(totalBedrag + " €", f2));
        document.add(preface5);
        document.add(new Paragraph(" "));


        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.addCell(getCell("\nBestek onder voorbehoud van vergissingen en/of wijzigingen.\n" +
                "Dit bestek is indicatief en wordt pas afgesloten na de herstelling.\n" +
                "Ø Niet elastische zone\n ", PdfPCell.ALIGN_CENTER));
        document.add(table);

        document.close();
        output.close();

        return myFile;

    }

    private PdfPTable createTable(){

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(100);
        table.setTotalWidth(document.getPageSize().getWidth());

        Font f = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, GrayColor.BLACK);

        PdfPCell cell;
        cell = new PdfPCell(new Phrase("Klant", f));
        cell.setColspan(2);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Voertuig", f));
        cell.setColspan(2);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Naam: " + naamKlant + "\nTelefoon: " + telefoonKlant + "\nEmail: " + emailKlant + "\nVerzekeraar: " + verzekeraarKlant));
        cell.setColspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Merk: " + merkVoertuig +"\nModel: " + modelVoertuig + "\nNummerplaat: " + nummerPlaatVoertuig + "\nChassisnummer: " + chassisNmrVoertuig + "\nKleur: " + kleurVoertuig));
        cell.setColspan(2);
        table.addCell(cell);

        return table;
    }

    public PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        //cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }
}
