package com.example.dell.hagelschade;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.AutoScrollHelper;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.hagelschade.viewpageradapter.ViewPagerAdapter;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ViewPagerActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private Button button;
    private String klantNaam;
    private String emailKlantEmail;
    private String emailHerstellerNaam;
    private String emailAdresHersteller;
    private String emailPostcodeHersteller;
    private String emailGemeentsteHersteller;
    private String emailOndernemersNrHersteller;



    private GetKlantName getKlantName;
    private ImageButton ibNext;
    private ImageButton ibPrevious;
    private ArrayList<File> file;
    private TextView tvSharedPics;
    private boolean isPictureShared = false;
    private int intViewPagerID = -1;
    //private Map<Integer, Integer> listPagerItems = new HashMap<>();
    private ArrayList<Uri> uriListForShare = new ArrayList<>();
    private LinkedHashMap<Integer,File> urisForShare = new LinkedHashMap<>();
    private LinkedHashMap<Integer, Integer> listPagerItems = new LinkedHashMap<>();
    private int pageSelected;
    private int tempValue = -1;
    private CheckBox cbSharedPictures;
    private boolean blShare = false;
    private MenuItem item;
    private MenuItem item2;
    private  boolean setFullScreen;
    private  ArrayList<String> strList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);


        viewPager = (ViewPager) findViewById(R.id.pager);
        cbSharedPictures = (CheckBox) findViewById(R.id.cbSharePicture);
        cbSharedPictures.setVisibility(View.INVISIBLE);
        cbSharedPictures.setBackgroundColor(Color.WHITE);

        cbSharedPictures.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setTextColor(Color.RED);
                    buttonView.setText("GEDEELD");

                }
                if (!isChecked) {
                    buttonView.setTextColor(Color.BLACK);
                    buttonView.setText("DELEN");
                }
            }
        });


       // tvSharedPics = (TextView) findViewById(R.id.tvGedeeld);
       // tvSharedPics.setVisibility(View.INVISIBLE);

        ibNext = (ImageButton) findViewById(R.id.btnNextPager);
        ibPrevious = (ImageButton) findViewById(R.id.btnPreviousPager);
        ibNext.setOnClickListener(this);
        ibPrevious.setOnClickListener(this);

        Intent intent = getIntent();
        file = (ArrayList<File>) getIntent().getSerializableExtra("files");
        klantNaam = intent.getStringExtra("klantnaam");
        emailKlantEmail = intent.getStringExtra("klantemail");
        emailHerstellerNaam = intent.getStringExtra("herstellernaam");
        emailAdresHersteller = intent.getStringExtra("herstelleradres");
        emailPostcodeHersteller = intent.getStringExtra("herstellerpostcode");
        emailGemeentsteHersteller = intent.getStringExtra("herstellergemeente");
        emailOndernemersNrHersteller = intent.getStringExtra("ondernemernrhersteller");
        setFullScreen = intent.getBooleanExtra("fullscreen", false);

        int fileSize = file.size();
        for (int i = 0; i < fileSize; i++) {
            listPagerItems.put(i, -1);
            //urisForShare.put(i, null);
            urisForShare.put(i, new File("dummytext"));
            Log.d("LISTDATA", urisForShare.get(i).toString());
        }

        for (int i = 0; i < file.size(); i++) {
            //urisForShare.add(i);
            Log.d("FILES", file.get(i).toString());
        }

       /* Log.d("LISTDATAAfter", listPagerItems.get(0).toString());
        if (listPagerItems.containsValue(5)) {
            Log.d("VALUE", "CONTAINS VALUE");
        } else {
            Log.d("VALUE", "CONTAINS VALUE NOT");
        }
        listPagerItems.put(0, 5);
        Log.d("LISTDATAAfter", listPagerItems.get(0).toString());
        if (listPagerItems.containsValue(5)) {
            Log.d("LISTTDATAVALUE", "CONTAINS VALUE");
        } else {
            Log.d("LISTDATAVALUE", "CONTAINS VALUE NOT");
        } */

        MyFragment fr = new MyFragment();
        FragmentManager fm = getSupportFragmentManager();
        getKlantName = (GetKlantName) fr;
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.my_fragment, fr);
        fragmentTransaction.commit();
        getKlantName.getKlantName(klantNaam);

        button = (Button) findViewById(R.id.btnClosePager);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(5);
                button.startAnimation(animation);

               /* Intent intent = new Intent(ViewPagerActivity.this, ListActivity.class);

                int size = urisForShare.size();
                for(int i = 0; i < size; i++){

                    String fotoFile = urisForShare.get(i).toString();
                    if(!fotoFile.matches("dummytext")){
                        stringUriForShare.add(fotoFile);
                        Log.d("FOTOURL", urisForShare.get(i).toString());
                    }
                }
                intent.putStringArrayListExtra("picturefilesforshare", stringUriForShare);
                startActivity(intent); */
                finish();
            }
        });


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageSelected = position;
                if (listPagerItems.containsValue(position)) {
                   //tvSharedPics.setVisibility(View.VISIBLE);
                    cbSharedPictures.setText("GEDEELD");
                    cbSharedPictures.setTextColor(Color.RED);
                    cbSharedPictures.setChecked(true);

                } else {
                    //tvSharedPics.setVisibility(View.INVISIBLE);
                    cbSharedPictures.setText("DELEN");
                    cbSharedPictures.setTextColor(Color.BLACK);
                    cbSharedPictures.setChecked(false);
                }


               /////////////////////////////////////////////////////////////////////////////
                // NOT WORKING
              /*  for(int i = 0; i < listPagerItems.size(); i++){
                    if(listPagerItems.get(i) == position){
                        tvSharedPics.setVisibility(View.VISIBLE);
                    }else{
                        tvSharedPics.setVisibility(View.INVISIBLE);
                    }
                } */

               /* if(intViewPagerID == position){
                    tvSharedPics.setVisibility(View.VISIBLE);
                }else{
                    tvSharedPics.setVisibility(View.INVISIBLE);
                } */
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        cbSharedPictures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int position = viewPager.getCurrentItem();

                if(cbSharedPictures.isChecked()){

                   // Toast.makeText(ViewPagerActivity.this, "PagerID = " + viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();
                   // if (!listPagerItems.containsValue(position)) {
                        //tvSharedPics.setVisibility(View.VISIBLE);
                        //cbSharedPictures.setText("GEDEELD");
                        //cbSharedPictures.setTextColor(Color.RED);
                        listPagerItems.put(position, position);
                        urisForShare.put(position, file.get(position));
                   // }

                }else{
                    listPagerItems.put(position, -1);
                    urisForShare.put(position, new File("dummytext"));
                    //Toast.makeText(ViewPagerActivity.this, "UNCHECQUED", Toast.LENGTH_LONG).show();
                    for(int i = 0; i < urisForShare.size(); i++){
                        //Log.d("UNCHECKED", urisForShare.get(i).toString());
                    }

                }


                /*   if (tempValue != viewPager.getCurrentItem()) {
                        //Toast.makeText(ViewPagerActivity.this, "PagerID = " + viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();

                       if (!listPagerItems.containsValue(position)) {
                            //tvSharedPics.setVisibility(View.VISIBLE);
                            cbSharedPictures.setText("GEDEELD");
                            cbSharedPictures.setTextColor(Color.RED);
                            listPagerItems.put(position, position);
                            urisForShare.put(position, file.get(position));
                        } else {
                            //tvSharedPics.setVisibility(View.INVISIBLE);
                            cbSharedPictures.setText("DELEN");
                            cbSharedPictures.setTextColor(Color.BLACK);
                            //cbSharedPictures.setChecked(false);
                            listPagerItems.put(position, -1);
                            urisForShare.put(position, null);
                        }
                    } else {
                        if (!listPagerItems.containsValue(position)) {
                            // tvSharedPics.setVisibility(View.VISIBLE);
                            cbSharedPictures.setText("GEDEELD");
                            cbSharedPictures.setTextColor(Color.RED);
                            listPagerItems.put(position, position);
                            urisForShare.put(position, file.get(position));
                        } else {
                            //tvSharedPics.setVisibility(View.INVISIBLE);
                            cbSharedPictures.setText("DELEN");
                            cbSharedPictures.setTextColor(Color.BLACK);
                            //cbSharedPictures.setChecked(false);
                            listPagerItems.put(position, -1);
                            urisForShare.put(position, null);
                        }



                    tempValue = viewPager.getCurrentItem(); */

            }
        });

        viewPagerAdapter = new ViewPagerAdapter(this, file);
        viewPager.setAdapter(viewPagerAdapter);

    }

    public void imageClick(View v){
        //Toast.makeText(ViewPagerActivity.this, "test image click", Toast.LENGTH_LONG).show();
        int id = viewPager.getCurrentItem();
        for(int i = 0; i < file.size(); i++){
            String temp = file.get(i).toString();
            strList.add(temp);
        }


        Intent intent = new Intent(ViewPagerActivity.this, FullscreenActivity.class);
        intent.putExtra("id", id);
        intent.putStringArrayListExtra("pics", strList);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPreviousPager: {
                if (viewPager.getCurrentItem() == 0) {
                    viewPager.setCurrentItem(file.size() - 1, true);
                } else {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                }
                break;
            }
            case R.id.btnNextPager: {
                if (viewPager.getCurrentItem() == file.size() - 1) {
                    viewPager.setCurrentItem(0, true);
                } else {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                }
                break;
            }
            default:
                break;
        }
    }

    public interface GetKlantName {
        void getKlantName(String name);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_share, menu);

        if(setFullScreen){
            item = menu.findItem(R.id.action_share);
            item.setVisible(true);

        }else{
            item = menu.findItem(R.id.action_share);
            item.setVisible(false);
        }

        item2 = menu.findItem(R.id.list_with_thumbs);
        item2.setVisible(false);




        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                if(!blShare){
                    cbSharedPictures.setVisibility(View.VISIBLE);
                    blShare = true;
                }else{
                    int size = urisForShare.size();
                    for(int i = 0; i < size; i++){
                        String fotoFile = urisForShare.get(i).toString();
                        if(!fotoFile.matches("dummytext")){
                            File file = new File(fotoFile);
                            Uri uriFile = Uri.fromFile(file);
                            uriListForShare.add(Uri.fromFile(file));
                            //Log.d("urisForShare", urisForShare.get(i).toString());
                         //   Log.d(" uriListForShare",  uriListForShare.get(i).toString());
                        }
                    }
                    if(uriListForShare.size() > 0){

                        Intent shareIntent;
                        //Uri uri = Uri.fromFile(myPDFFile);
                        shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                        shareIntent.setType("*/*");
                        shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailKlantEmail});
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Bestek Hagelschade!");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "Beste " + klantNaam + "\n\nIn bijlage vindt u de foto's van de hagelschade.\n\nMet vriendelijke groeten\n\nDe hersteller" +
                               "\n\nNaam: " + emailHerstellerNaam + "\nAdres: " + emailAdresHersteller + "\nPostcode: " + emailPostcodeHersteller + "\nGemeente: " + emailGemeentsteHersteller + "\nOndernemingsnummer: " + emailOndernemersNrHersteller);
                 // werkt       //shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///storage/emulated/0/DCIM/hagelschade/Geert%20van%20den%20Berghe%20/IMG_20170612_181110.jpg"));
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uriListForShare);

                        int fileSize = file.size();
                        for (int i = 0; i < fileSize; i++) {
                            listPagerItems.put(i, -1);
                            //urisForShare.put(i, null);
                            urisForShare.put(i, new File("dummytext"));
                            Log.d("LISTDATA", urisForShare.get(i).toString());
                        }
                        cbSharedPictures.setVisibility(View.INVISIBLE);
                        cbSharedPictures.setText("DELEN");
                        cbSharedPictures.setTextColor(Color.BLACK);
                        cbSharedPictures.setChecked(false);
                        blShare = false;

                        //Uri test = Uri.parse("file:///storage/emulated/0/DCIM/hagelschade/Geert%20van%20den%20Berghe%20/IMG_20170612_181110.jpg");
                        startActivity(Intent.createChooser(shareIntent, "Select"));
                        uriListForShare.clear();
                    }else{
                        Toast.makeText(ViewPagerActivity.this,"Gelieve een foto te selecteren!!!",Toast.LENGTH_LONG).show();
                    }
                }

            /*    int position = viewPager.getCurrentItem();
                if (tempValue != viewPager.getCurrentItem()) {
                    //Toast.makeText(ViewPagerActivity.this, "PagerID = " + viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();

                    if (!listPagerItems.containsValue(position)) {
                        //tvSharedPics.setVisibility(View.VISIBLE);
                        listPagerItems.put(position, position);
                        urisForShare.put(position, file.get(position));
                    } else {
                        //tvSharedPics.setVisibility(View.INVISIBLE);
                        listPagerItems.put(position, -1);
                        urisForShare.put(position, null);
                    }
                } else {
                    if (!listPagerItems.containsValue(position)) {
                       // tvSharedPics.setVisibility(View.VISIBLE);
                        listPagerItems.put(position, position);
                        urisForShare.put(position, file.get(position));
                    } else {
                        //tvSharedPics.setVisibility(View.INVISIBLE);
                        listPagerItems.put(position, -1);
                        urisForShare.put(position, null);
                    }

                }

                tempValue = viewPager.getCurrentItem(); */



            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

           /*     Toast.makeText(ViewPagerActivity.this, "PagerID = " + viewPager.getCurrentItem(), Toast.LENGTH_LONG).show();
                int position = viewPager.getCurrentItem();
                if(listPagerItems.containsValue(position)){
                    tvSharedPics.setVisibility(View.VISIBLE);
                    listPagerItems.remove(position);
                }else{
                    tvSharedPics.setVisibility(View.INVISIBLE);
                    listPagerItems.put(position, 1);

                } */


                /*if (!isPictureShared) {
                    tvSharedPics.setVisibility(View.VISIBLE);
                    //intViewPagerID = position;
                    listPagerItems.put(position, position);
                    isPictureShared = true;
                } else {
                    tvSharedPics.setVisibility((View.INVISIBLE));
                    listPagerItems.remove(position);
                    isPictureShared = false;
                    //intViewPagerID = -1;
                } */

                //viewPager.
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(blShare == true){
            cbSharedPictures.setVisibility(View.INVISIBLE);
            blShare = false;
            int fileSize = file.size();
            for (int i = 0; i < fileSize; i++) {
                listPagerItems.put(i, -1);
                //urisForShare.put(i, null);
                urisForShare.put(i, new File("dummytext"));
            }
            cbSharedPictures.setChecked(false);
            cbSharedPictures.setText("DELEN");
            cbSharedPictures.setTextColor(Color.BLACK);
        }else{
            finish();
        }
    }
}
