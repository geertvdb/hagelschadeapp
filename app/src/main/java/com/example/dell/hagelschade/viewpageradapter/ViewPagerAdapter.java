package com.example.dell.hagelschade.viewpageradapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.res.ConfigurationHelper;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.dell.hagelschade.FilePath;
import com.example.dell.hagelschade.FullscreenActivity;
import com.example.dell.hagelschade.MainActivity;
import com.example.dell.hagelschade.R;
import com.example.dell.hagelschade.model.Bestek;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Dell on 9/06/2017.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private Activity activity;
    private ArrayList<File> pics;
    private LayoutInflater inflater;
    private CheckBox checkBox;
    private String filePath;

    public ViewPagerAdapter(Activity activity, ArrayList<File> pics) {
        this.activity = activity;
        this.pics = pics;
    }

    @Override
    public int getCount() {
        return pics.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View itemView = inflater.inflate(R.layout.viewpager_item, container, false);

       itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

             //Toast.makeText(activity, "test viewpager click " + pics.get(position), Toast.LENGTH_LONG).show();
             /*   AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setTitle("DELEN")
                        .setMessage("Selecteren voor delen?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                //getCookieBarTop("Data opgeslagen...", "Bestek klant : " +_klant_naam);

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(MainActivity.this, "Er zijn geen gegevens opgeslagen!!!!", Toast.LENGTH_LONG).show();
                                //getCookieBarTop("Er zijn geen gegevens opgeslagen!", "");
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show(); */
                return true;
            }
        });



        ImageView img = (ImageView) itemView.findViewById(R.id.imagePager);
        //checkBox = (CheckBox) itemView.findViewById(R.id.cbSharePicture);
        //File file = new File(Environment.getExternalStorageDirectory() + "hagelschade/Dirk de Poeck/" + pics[position]);
        File file = pics.get(position);
        //String file = Environment.getExternalStorageDirectory() + "hagelschade/Dirk de Poeck/" + pics[position];
        //Glide.with(activity).from().into(img);
        //String file = "file://" + pics[position];
        Uri uri = Uri.fromFile(file);
        filePath = FilePath.getPath(activity, uri);
        Picasso.with(activity).load(file).into(img);
       // Picasso.with(activity).load("file://" + filePath).into(img); //WERKEND
                //Glide.with(activity).load(uri).into(img);

        /*ImageView img = (ImageView) itemView.findViewById(R.id.imagePager);
        int photoId = activity.getResources().getIdentifier(pics[position], "drawable", activity.getPackageName());
        img.setImageResource(photoId); */

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewGroup)container).removeView((View)object);
    }
}
